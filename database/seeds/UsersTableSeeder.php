<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Foundation\Auth\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create(array(
            'name' => 'nicolasvh',
//            'firstname' => 'Nicolas',
//            'lastname' => 'Vanhecke',
//            'sex' => 'm',
//            'birthday' => '1992-01-16',
//            'photo' => '',
//            'cellphone' => '0494047009',
//            'rank' => 'C+15/4',
            'email' => 'vanhecke.nicolas@hotmail.com',
            'password' => bcrypt('nicolasvh'),
            'remember_token' => str_random(10),
        ));

        $faker = Faker::create();

        foreach(range(1,25) as $index){
            $user = User::create([
                'name' => $faker->userName,
//                'firstname' => $faker->firstNameMale,
//                'lastname' => $faker->lastName,
//                'sex' => 'm',
//                'birthday' => $faker->dateTime,
//                'photo' => $faker->image(),
//                'cellphone' => $faker->phoneNumber,
//                'rank' => $faker->randomElement($ranks),
                'email' => $faker->email,
                'password' => bcrypt(str_random(10)),
                'remember_token' => str_random(10),
            ]);
        }

    }
}
