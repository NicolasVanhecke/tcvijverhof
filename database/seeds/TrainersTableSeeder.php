<?php

use Illuminate\Database\Seeder;

class TrainersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $trainers = [
            ['firstname'=> 'Christos', 'lastname'=> 'Baltogiannis',
                'niveau'=> 'International trainer',
                'photo'=> '',
                'cellphone'=> '0487 46 55 77',
                'email'=> 'christos@tcv.be',
                'stroke'=> 'Backhand slice',
                'likes'=> 'de tegenstander zot maken',
            ],
            ['firstname'=> 'Christophe', 'lastname'=> 'Vanhecke',
                'niveau'=> 'Instructeur B',
                'photo'=> '',
                'cellphone'=> '0487 32 35 37',
                'email'=> 'christophe@tcv.be',
                'stroke'=> 'Backhand',
                'likes'=> 'Sara haar patekes',
            ],
            ['firstname'=> 'Eduardo', 'lastname'=> 'Peralta-Tello',
                'niveau'=> 'International trainer',
                'photo'=> '',
                'cellphone'=> '0486 123 577',
                'email'=> 'eduardo@tcv.be',
                'stroke'=> 'Backhand down the line',
                'likes'=> 'sparring met Nadal',
            ],
            ['firstname'=> 'Jolien', 'lastname'=> 'De Brabander',
                'niveau'=> 'Instructeur B',
                'photo'=> '',
                'cellphone'=> '0477 36 22 12',
                'email'=> 'jolien@tcv.be',
                'stroke'=> 'Volley',
                'likes'=> 'Gossip Girl kijken in bad met een glaasje wijn',
            ],
            ['firstname'=> 'Kim', 'lastname'=> 'Demeyer',
                'niveau'=> 'Trainer',
                'photo'=> '',
                'cellphone'=> '0487 26 51 77',
                'email'=> 'kim@tcv.be',
                'stroke'=> 'Service slice',
                'likes'=> 'een sigaretje tussendoor',
            ],
            ['firstname'=> 'Yannis', 'lastname'=> 'Baltogiannis',
                'niveau'=> 'Jeugdtrainer',
                'photo'=> '',
                'cellphone'=> '0487 12 51 09',
                'email'=> 'yannis@tcv.be',
                'stroke'=> 'Dropshot',
                'likes'=> 'uitgaan en niet weten waar je wakker wordt',
            ],
            ['firstname'=> 'Thomas', 'lastname'=> 'Ackerman',
                'niveau'=> 'Jeugdtrainer',
                'photo'=> '',
                'cellphone'=> '0496 495 077',
                'email'=> 'thomas@tcv.be',
                'stroke'=> 'Forehand cross',
                'likes'=> 'zichzelf',
            ],
            ['firstname'=> 'Tim', 'lastname'=> 'Wauters',
                'niveau'=> 'Jeugdtrainer',
                'photo'=> '',
                'cellphone'=> '0483 43 12 74',
                'email'=> 'tim@tcv.be',
                'stroke'=> 'Dropshot',
                'likes'=> 'de pintjes na de training',
            ],
            ['firstname'=> 'Quentin', 'lastname'=> 'Devos',
                'niveau'=> 'Aspirant initiator',
                'photo'=> '',
                'cellphone'=> '0487 46 55 77',
                'email'=> 'quentin@tcv.be',
                'stroke'=> 'Aanvallende forehand',
                'likes'=> 'als zijn tindermeisjes',
            ],
            ['firstname'=> 'Wiebe', 'lastname'=> 'Van Mullem',
                'niveau'=> 'Aspirant initiator',
                'photo'=> '',
                'cellphone'=> '0487 377 998',
                'email'=> 'wiebe@tcv.be',
                'stroke'=> 'Geen enkele',
                'likes'=> 'mensen die op hun eigen handen zitten',
            ],
        ];

        DB::table('trainers')->insert($trainers);
    }
}
