<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            ['name'=> 'medewerker', 'description'=> 'Toegang tot beperkte backend',],
            ['name'=> 'baas', 'description'=> 'Extra functies ivm rollen toewijzen',],
            ['name'=> 'admin', 'description'=> 'The one to rule them all',],
        ];

        DB::table('roles')->insert($roles);
    }
}
