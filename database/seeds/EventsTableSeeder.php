<?php

use Illuminate\Database\Seeder;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $events = [
            ['name'=> 'Glow in the dark tennis',
                'pre'=> 'Dacht je dat licht nodig was voor tennis? Mis! Tennissen in het donker met fluokledij is zeer hip, en wij doen mee!',
                'body'=> 'Given the array of highly-competitive professional tournaments spread around the globe, it\'s becoming increasingly difficult for tennis chiefs to continue to raise the bar. In Roger Federer, Novak Djokovic, Rafael Nadal and Andy Murray it\'s already been observed the four of the game\'s greats are around at the same time and fortunate enough to compete together. Maria Sharapova and Serena Williams are also great talents and as glamorous as their records are impressive, so the sport surely has all bases already covered, no?',
                'date'=> '23-02-16',
                'price'=> 'null',
                'afterword'=> 'My partner was about to get himself shot. I intervened. He was angry because those two dealers of yours had just murdered an eleven year-old boy. Then again, maybe he thought it was you who gave the order.',
            ],
            ['name'=> 'Jaarlijks sluitingsfeest',
                'pre'=> 'Om ons tennisjaar mooi af te sluiten geven wij jaarlijks een groot feest. Tal van leden zullen aanwezig zijn, jij toch ook?',
                'body'=> 'Op zaterdag 5 december zal de tennishal vanTC Vijverhof weer omgetoverd worden tot een feestzaal. Dit voor het jaarlijkse sluitingsfeest. Lekker eten, feestelijke muziek en ambience gegarandeerd. We hebben er een mooi jaar opzitten met onder andere succesvole tornooienm interclubfinales nationaal zowel bij de jeugd als bij volwassenenm een succesvolle editie van de Rising Stars en nog woveel meer... Een jaar om feetelijk af te sluiten dus. Iedereen is uiteraard welkom! Nodig gerust kennissen en vrienden uit. Leden met een hart voor de club mogen niet ontbreken. SAVE THE DATE!',
                'date'=> '21-01-16',
                'price'=> 'null',
                'afterword'=> 'He has enough money to last forever. He knows he needs to keep moving. You\'ll never find him. He\'s out of the picture. I saved his life, I owed him that, but now he and I are done. Which is exactly what you wanted, isn\'t it. You\'ve always struck me as a very pragmatic man so if I may, I would like to review options with you. Of which, it seems to me you have two. .',
            ],
        ];

        DB::table('events')->insert($events);
    }
}
