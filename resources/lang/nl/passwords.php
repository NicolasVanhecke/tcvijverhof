<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Wachtwoorden moeten overeen komen en minimum 6 tekens lang zijn',
    'reset' => 'Je wachtwoord is opnieuw ingesteld',
    'sent' => 'Een email is verstuurd om je wachtwoord opnieuw in te stellen',
    'token' => 'Deze token is ongeldig.',
    'user' => "Ongeldige gegevens, probeer het opnieuw.",

];
