<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <title>@yield('title')</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,300,700' rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Play' rel='stylesheet' type='text/css'>
    <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet">

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
    <script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyClqFiPU87nYlDWmDR-1gB7UPI3M8FqqIQ"></script>
    <script src="{{ asset('assets/js/app.js') }}"></script>
</head>

<body id="app-layout">
<nav class="navbar navbar-default hidden-lg hidden-md">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand hidden-sm" href="#">@yield('title')</a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            {{--<ul class="nav navbar-nav">--}}
{{--                <li><a href="{{ url('/') }}">Home</a></li>--}}
            {{--</ul>--}}

            <ul class="nav navbar-nav navbar-right">
                @if (Auth::guest())
                    <li class="{{ Request::is('/') ? 'active' : '' }}"><a href="{{ url('/') }}">Home</a></li>
                    <li class="{{ Request::is('blog*') ? 'active' : '' }}"><a href="{{ url('blog') }}">Blog</a></li>
                    <li class="{{ Request::is('evenementen*') ? 'active' : '' }}"><a href="{{ url('evenementen') }}">Evenementen</a></li>
                    <li class="{{ Request::is('tennisschool*') ? 'active' : '' }}"><a href="{{ url('tennisschool') }}">Tennisschool</a></li>
                    <li class="{{ Request::is('club*') ? 'active' : '' }}"><a href="{{ url('club') }}">Club</a></li>
                    <li class="{{ Request::is('contact*') ? 'active' : '' }}"><a href="{{ url('contact') }}">Contact</a></li>
                    <li class="{{ Request::is('login*') ? 'active' : '' }}"><a href="{{ url('/login') }}">Login</a></li>
                @else
                    <li class="{{ Request::is('/') ? 'active' : '' }}"><a href="{{ url('/') }}">Home</a></li>
                    <li class="{{ Request::is('blog*') ? 'active' : '' }}"><a href="{{ url('blog') }}">Blog</a></li>
                    <li class="{{ Request::is('evenementen*') ? 'active' : '' }}"><a href="{{ url('evenementen') }}">Evenementen</a></li>
                    <li class="{{ Request::is('tennisschool*') ? 'active' : '' }}"><a href="{{ url('tennisschool') }}">Tennisschool</a></li>
                    <li class="{{ Request::is('club*') ? 'active' : '' }}"><a href="{{ url('club') }}">Club</a></li>
                    <li><a href="http://www.tennisvlaanderen.be/home?p_p_state=maximized&p_p_mode=view&saveLastPath=0&_58_struts_action=%2Flogin%2Flogin&p_p_id=58&p_p_lifecycle=0&_58_redirect=%2Freserveer-een-terrein">Terreinreservatie</a></li>
                    <li class="{{ Request::is('contact*') ? 'active' : '' }}"><a href="{{ url('contact') }}">Contact</a></li>
                    <li class="{{ Request::is('logout*') ? 'active' : '' }}"><a href="{{ url('/logout') }}">Logout</a></li>
                @endif
            </ul>
        </div>
    </div>
</nav>


<div class="row">
    <div id="sidebar" class="col-lg-2 col-md-2 hidden-sm hidden-xs">
        <nav id="nav">
            <ul class="nav-ul">
            @if (Auth::guest())
                <li class="nav-li {{ Request::is('/') ? 'active' : '' }}"><a href="{{ URL::to('/') }}"> Home </a></li>
                <li class="nav-li {{ Request::is('blog*') ? 'active' : '' }}"><a href="{{ URL::to('blog') }}"> Blog </a></li>
                <li class="nav-li {{ Request::is('evenementen*') ? 'active' : '' }}"><a href="{{ URL::to('evenementen') }}"> Evenementen </a></li>
                <li class="nav-li {{ Request::is('tennisschool*') ? 'active' : '' }}"><a href="{{ URL::to('tennisschool') }}"> Tennisschool </a></li>
                <li class="nav-li {{ Request::is('club*') ? 'active' : '' }}"><a href="{{ URL::to('club') }}"> Club </a></li>
                <li class="nav-li {{ Request::is('contact*') ? 'active' : '' }}"><a href="{{ URL::to('contact') }}"> Contact </a></li>
                <li class="nav-li {{ Request::is('login*') ? 'active' : '' }}"><a href="{{ URL::to('login') }}"> Login </a></li>
            @else
                <li class="nav-li {{ Request::is('/') ? 'active' : '' }}"><a href="{{ URL::to('/') }}"> Home </a></li>
                <li class="nav-li {{ Request::is('blog*') ? 'active' : '' }}"><a href="{{ URL::to('blog') }}"> Blog </a></li>
                <li class="nav-li {{ Request::is('evenementen*') ? 'active' : '' }}"><a href="{{ URL::to('evenementen') }}"> Evenementen </a></li>
                <li class="nav-li {{ Request::is('tennisschool*') ? 'active' : '' }}"><a href="{{ URL::to('tennisschool') }}"> Tennisschool </a></li>
                <li class="nav-li {{ Request::is('club*') ? 'active' : '' }}"><a href="{{ URL::to('club') }}"> Club </a></li>
                <li class="nav-li {{ Request::is('contact*') ? 'active' : '' }}"><a href="{{ URL::to('contact') }}"> Contact </a></li>
                <li class="nav-li {{ Request::is('logout*') ? 'active' : '' }}"><a href="{{ URL::to('logout') }}"> Logout </a></li>
            @endif
            </ulclass>
        </nav>
    </div>
    <div class="reservation col-lg-2 col-md-2 hidden-sm hidden-xs" onclick="location.href='http://www.tennisvlaanderen.be/home?p_p_state=maximized&p_p_mode=view&saveLastPath=0&_58_struts_action=%2Flogin%2Flogin&p_p_id=58&p_p_lifecycle=0&_58_redirect=%2Freserveer-een-terrein';">
        <p> Reserveer een terrein</p>
    </div>

    @yield('content')

</div>



</body>
</html>
