<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <title>@yield('title')</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,300,700' rel='stylesheet' type='text/css'>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Play' rel='stylesheet' type='text/css'>
    <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet">

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
    <script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyClqFiPU87nYlDWmDR-1gB7UPI3M8FqqIQ"></script>
    <script src="//cdn.ckeditor.com/4.5.3/standard/ckeditor.js"></script>
    <script src="{{ asset('assets/js/bootbox.js') }}"></script>
    <script src="{{ asset('assets/js/app.js') }}"></script>
</head>

<body id="app-layout">
<nav class="navbar navbar-default hidden-lg hidden-md hidden-sm">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            {{--<a class="navbar-brand" href="#">Laravel</a>--}}
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            {{--<ul class="nav navbar-nav">--}}
{{--                <li><a href="{{ url('/') }}">Home</a></li>--}}
            {{--</ul>--}}

            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ URL::to('/backend/dash') }}"> Dashboard </a></li>
                <li><a href="{{ URL::to('/backend/blog') }}"> Blog </a></li>
                <li><a href="{{ URL::to('/backend/evenementen') }}"> Evenementen </a></li>
                <li><a href="{{ URL::to('/backend/trainers') }}"> Trainers </a></li>
                <li><a href="{{ URL::to('/backend/berichten') }}"> Berichten </a></li>
                <li><a href="{{ URL::to('/backend/inschrijvingen') }}"> Inschrijvingen </a></li>
                <li><a href="{{ URL::to('/backend/gebruikers') }}"> Gebruikers </a></li>
                <li><a href="{{ URL::to('/backend/rollen') }}"> Rollen </a></li>
                <li><a href="{{ URL::to('/') }}"> Frontend </a></li>
                <li><a href="{{ URL::to('logout') }}"> Logout </a></li>
            </ul>
        </div>
    </div>
</nav>


<div class="row">
    <div id="sidebar" class="col-lg-2 col-md-2 col-sm-2 hidden-xs">
        <nav id="nav">
            <ul>
                <li class="{{ Request::is('backend/dash*') ? 'active' : '' }}"><a href="{{ URL::to('/backend/dash') }}"> Dashboard </a></li>
                <li class="{{ Request::is('backend/blog*') ? 'active' : '' }}"><a href="{{ URL::to('/backend/blog') }}"> Blog </a></li>
                <li class="{{ Request::is('backend/evenementen*') ? 'active' : '' }}"><a href="{{ URL::to('/backend/evenementen') }}"> Evenementen </a></li>
                <li class="{{ Request::is('backend/trainers*') ? 'active' : '' }}"><a href="{{ URL::to('/backend/trainers') }}"> Trainers </a></li>
                <li class="{{ Request::is('backend/berichten*') ? 'active' : '' }}"><a href="{{ URL::to('/backend/berichten') }}"> Berichten </a></li>
                <li class="{{ Request::is('backend/inschrijvingen*') ? 'active' : '' }}"><a href="{{ URL::to('/backend/inschrijvingen') }}"> Inschrijvingen </a></li>
                <li class="{{ Request::is('backend/gebruikers*') ? 'active' : '' }}"><a href="{{ URL::to('/backend/gebruikers') }}"> Gebruikers </a></li>
                <li class="{{ Request::is('backend/rollen*') ? 'active' : '' }}"><a href="{{ URL::to('/backend/rollen') }}"> Rollen </a></li>
                <li><span style="opacity: 0;" >seperator</span></li>
                {{--<li><a href="{{ URL::to('/') }}"> Frontend </a></li>--}}
                <li class="{{ Request::is('logout') ? 'active' : '' }}"><a href="{{ URL::to('logout') }}"> Logout </a></li>
            </ul>
        </nav>
    </div>
    <div class="reservation col-lg-2 col-md-2 col-sm-2 hidden-xs" onclick="location.href='{{ URL::to('/') }}';">
        <p> Frontend </p>
    </div>

    @yield('content')

</div>



</body>
</html>
