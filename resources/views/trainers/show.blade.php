@extends('layouts.index')
@section('title', 'Trainers')

@section('sidemenu') @endsection

{{--   PAGE STRUCTURE | LAYOUTS/INDEX  --}}
{{--<div class="row">--}}
{{--<div id="sidebar" class="col-lg-1 col-md-1 col-sm-1 hidden-xs"> </div>--}}
{{--@section('content')--}}
{{--</div>--}}

@section('content')

    <div class="row">
        <div id="content" class="col-lg-8 col-lg-offset-3 col-md-8 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
            <div class="row">

                <h2>{{ $trainer->firstname }} {{ $trainer->lastname }}</h2>
                <img class="hidden-lg hidden-md col-sm-12 col-xs-12" src="{{ asset('assets/photos/'.$trainer->photo) }}" alt="">
                <div class="col-lg-7 col-md-7 col-sm-12 col-sm-12">

                    <table class="table table-detail">
                        <tbody>
                        <tr>
                            <th>Niveau</th>
                            <td>{{ $trainer->niveau }}</td>
                        </tr>
                        <tr>
                            <th>E-mail</th>
                            <td>{{ $trainer->email }}</td>
                        </tr>
                        <tr>
                            <th>GSM-nummer</th>
                            <td>{{ $trainer->cellphone }}</td>
                        </tr>
                        <tr>
                            <th>Favoriete slag</th>
                            <td>{{ $trainer->stroke }}</td>
                        </tr>
                        <tr>
                            <th>Houdt van</th>
                            <td>{!! $trainer->likes !!}</td>
                        </tr>
                        {{--<tr>--}}
                            {{--<th>Extra</th>--}}
                            {{--<td>{{ $trainer->message }}</td>--}}
                        {{--</tr>--}}
                        </tbody>
                    </table>
                </div>

                <img class="col-lg-5 col-md-5 hidden-sm hidden-xs" src="{{ asset('assets/photos/'.$trainer->photo) }}" alt="">

            </div>

        </div>
    </div>

@endsection


