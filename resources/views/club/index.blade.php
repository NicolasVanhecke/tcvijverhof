@extends('layouts.index')
@section('title', 'Clubhuis')

@section('sidemenu') @endsection

{{--   PAGE STRUCTURE | LAYOUTS/INDEX  --}}
{{--<div class="row">--}}
{{--<div id="sidebar" class="col-lg-1 col-md-1 col-sm-1 hidden-xs"> </div>--}}
{{--@section('content')--}}
{{--</div>--}}

@section('content')

    <div class="row">
        <div id="content" class="col-lg-8 col-lg-offset-3 col-md-8 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
            <div class="row">

                @if (Session::has('message'))
                    <div class="alert">{{ Session::get('message') }}</div>
                @endif

                <div class="section courts">
                    <h2>Onze gravel- en hardcourt terreinen</h2>

                    <div class="descr">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"><i class="fa fa-sun-o"></i>6 gravel banen</div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"><i class="fa fa-tint"></i>3 hardcourt banen</div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12"><i class="fa fa-exclamation"></i>2 gravel banen in ballon</div>
                    </div>

                    {{--<div class="row subsectionLeft">--}}
                        {{--<p class="col-lg-12">--}}
                            {{--<img src="{{ asset('assets/images/gravelCourt.png') }}" alt="" />--}}
                            {{--Onze centercourt banen zijn aangelegd met een speciale onderlaag die ervoor zorgt dat je nooit lang hoeft te wachten op droge, bespeelbare terreinen. Onze banen delen deze techniek met de gravelcourts van Rolan Garros. We zijn hier zeer fier op en verzorgen onze terrein dus zeer goed. <br /> <br />--}}
                            {{--Deze techniek is gebruikt in het zelbekende Roland-Garros. We zijn daarom ook zeer fier op onze gravel courts en doe er alles aan om deze in superconditie te houden het hele jaar door. Tijdens het winterseizoen komt er tijdelijk een ballon over deze twee banen. De ballon verlicht en verwarmd, zo kunnen leden het hele jaar door op gravel spelen.--}}
                        {{--</p>--}}
                    {{--</div>--}}

                    {{--<div class="row subsectionRight">--}}
                        {{--<p class="col-lg-12">--}}
                            {{--<img src="{{ asset('assets/images/indoorCourt.jpg') }}" alt="" />--}}
                            {{--De center court banen in TC Vijverhof bestaan uit wit kalksteen met daaroverheen enkele millimeters gravel. Hieronder bevindt zich een laag van 15 centimeter aan vulkanisch gesteente gevolgd door een kleine meter zand en een plaat beton. De vulkanische laag zorgt ervoor dat de terreinen een zeer goede drainage hebben. Tijdens het zomerseizoen kan er meestal al gespeeld worden nadat de terreinen slechts een half uurtje geen regen hebben gezien. <br /> <br />--}}
                            {{--Deze techniek is gebruikt in het zelbekende Roland-Garros. We zijn daarom ook zeer fier op onze gravel courts en doe er alles aan om deze in superconditie te houden het hele jaar door. Tijdens het winterseizoen komt er tijdelijk een ballon over deze twee banen. De ballon verlicht en verwarmd, zo kunnen leden het hele jaar door op gravel spelen.--}}
                        {{--</p>--}}
                    {{--</div>--}}
                </div>

                <div class="row subsection gravel">
                    <img class="col-lg-5 col-md-5 col-sm-12 col-xs-12 pull-left" src="{{ asset('assets/images/gravelCourt.png') }}" alt="" />
                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                        <h4>GRAVEL</h4>
                        <p>We beschikken over 6 mooie gravelbanen die het helezomerseizoen beschikbaar zijn. Tijdens het winterseizoen wordt een ballon getrokken over onze twee centercourts. Zo kunnen leden het hele jaar door op gravel trainen. <br /><br />
                        Onze terreinen zijn aangelegd om snel bepseelbaar te zijn na de winter en zelfs na een regenbui in de zomermaanden.</p>
                    </div>
                </div>

                <div class="row subsection indoor">
                    <img class="col-lg-5 col-md-5 col-sm-12 col-xs-12 pull-right" src="{{ asset('assets/images/indoorCourt.jpg') }}" alt="" />
                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                        <h4>INDOOR</h4>
                        <p>Ook indoor in onze zaal hebben wij 3 zeer mooie hardcourt banen. Deze zaal blijft het hele jaar door staan, zelfs bij regen tijdens de zomermaanden kan er dus binnen gespeeld worden (zonder extra kosten voor onze leden).<br /><br />
                            De harcourt bevat een extra dempende laag die zeer aangenaam is voor de knieën en gewrichten.</p>
                    </div>
                </div>


                <div class="row section clubhouse">
                    <h2>Onze gezellige zomer- en winterbar</h2>
                    {{--<p>Op zaterdag 5 december zal de tennishal van TC Vijverhof weer omgetoverd worden tot een feestzaal. Dit voor het jaarlijkse sluitingsfeest. Lekker eten, feestelijke muziek en ambience gegarandeerd. We hebben er een mooi jaar opzitten met onder andere succesvole tornooienm interclubfinales nationaal zowel bij de jeugd als bij volwassenenm een succesvolle editie van de Rising Stars en nog woveel meer... Een jaar om feetelijk af te sluiten dus. Iedereen is uiteraard welkom! Nodig gerust kennissen en vrienden uit. Leden met een hart voor de club mogen niet ontbreken. SAVE THE DATE! </p>--}}
                    {{--<div class="images">--}}
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 chimg"><img class="img-responsive" src="{{ asset('assets/images/bar1.jpg') }}" alt="" /></div>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 chimg"><img class="img-responsive" src="{{ asset('assets/images/bar2.jpg') }}" alt="" /></div>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 chimg"><img class="img-responsive" src="{{ asset('assets/images/bar3.jpg') }}" alt="" /></div>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 chimg"><img class="img-responsive" src="{{ asset('assets/images/bar4.jpg') }}" alt="" /></div>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 chimg"><img class="img-responsive" src="{{ asset('assets/images/bar5.jpg') }}" alt="" /></div>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 chimg"><img class="img-responsive" src="{{ asset('assets/images/bar6.jpg') }}" alt="" /></div>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 chimg"><img class="img-responsive" src="{{ asset('assets/images/bar7.jpg') }}" alt="" /></div>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 chimg"><img class="img-responsive" src="{{ asset('assets/images/bar8.jpg') }}" alt="" /></div>
                    {{--</div>--}}
                </div>

                <div class="section eventlocation">
                    <h2>Huur onze zomerbar voor privé evenementen</h2>
                    <p>De zomer- of winterbar van TC Vijverhof kan afgehuurd worden voor evenementen. De locatie is ideaal voor bedrijfsfeesten, communies, verjaardagen, etc.. U kan genieten van het gezellige huisje in combinatie met het speelplein, de petanquebaan, de groene tuin en zicht op de mooie vijver. U kan ook de combinatie maken met het huren van enkele tennisbanen. <br /> <br />
                        Klinkt interessant? Neem gerust contact op met het nummer 0495 53 66 55</p>
                </div>

            </div>
        </div>
    </div>

@endsection
