@extends('layouts.index')
@section('title', 'Blog')

@section('sidemenu') @endsection

{{--   PAGE STRUCTURE | LAYOUTS/INDEX  --}}
{{--<div class="row">--}}
    {{--<div id="sidebar" class="col-lg-1 col-md-1 col-sm-1 hidden-xs"> </div>--}}
    {{--@section('content')--}}
{{--</div>--}}

@section('content')

<div class="row">
    <div id="content" class="col-lg-8 col-lg-offset-3 col-md-8 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
        <div class="row">

            @if (Session::has('message'))
                <div class="alert">{{ Session::get('message') }}</div>
            @endif

            @foreach($blog as $x)
                <div class="section post">
                    <h2 onclick="location.href='{{ URL::to('blog/' . $x->id) }}';">{{ $x->name }}</h2>
                    <p class="pre"><span>{{ $x->date }}</span> {{ $x->pre }}</p>
                    {{--<p>{{ html_entity_decode(stripslashes($x->body)) }}</p>--}}
                    @if( $x->photos )
                        <p class="body">
                            <img class="pull-right" src="{{ $x->photos }}" alt="" />
                            {!!  $x->body !!}
                        </p>
                    @else
                        <p>{!! $x->body !!}</p>
                    @endif
                    <div class="sub pull-right">
                        {{--<i class="fa fa-share" onclick="location.href='https://www.facebook.com/dialog/share?app_id=213971105620142&display=popup&href=http://www.nicolasvanhecke.be/public/blog/{{ $x->id }}&redirect_uri=http://www.nicolasvanhecke.be/public/blog';" >Share</i>--}}
                        <i class="fa fa-share"></i><a href="https://www.facebook.com/dialog/share?app_id=213971105620142&display=popup&href=http://www.nicolasvanhecke.be/public/blog/" . {{ $x->id }} . "&redirect_uri=http://www.nicolasvanhecke.be/public/blog/">Share</a>
                        <i class="fa fa-comment"></i><a href="{{ URL::to('/blog/' . $x->id . '#reageer') }}">Comment</a>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
</div>

@endsection
