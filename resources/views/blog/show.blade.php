@extends('layouts.index')
@section('title', 'Blog')

@section('sidemenu') @endsection

{{--   PAGE STRUCTURE | LAYOUTS/INDEX  --}}
{{--<div class="row">--}}
    {{--<div id="sidebar" class="col-lg-1 col-md-1 col-sm-1 hidden-xs"> </div>--}}
    {{--@section('content')--}}
{{--</div>--}}

@section('content')

<div class="row">
    <div id="content" class="col-lg-8 col-lg-offset-3 col-md-8 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
        <div class="row">

            @if (Session::has('message'))
                <div class="alert alert-succes">{{ Session::get('message') }}</div>
            @endif

            @if ($errors->has())
                <div class="alert alert-error">
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br />
                    @endforeach
                </div>
            @endif

            <div class="section post">
                <h2>{{ $blog->name }}</h2>
                <p class="pre"><span>{{ $blog->date }}</span> {{ $blog->pre }}</p>
                {{--<p>{{ html_entity_decode(stripslashes($x->body)) }}</p>--}}
                @if( $blog->photos )
                    <div class="body">
                        <img class="pull-right" src="{{ $blog->photos }}" alt="" />
                        {!! $blog->body !!}
                    </div>
                @else
                    <div class="body">{!! $blog->body !!}</div>
                @endif
            </div>

            {{--@if(empty($blog->comments))--}}
            @if($commentsnr != 0)
                <div id="comment" class="section comments">
                    <h2>Reacties</h2>
                    @foreach($blog->comments as $x)
                        @if(Auth::user() && $x->user_id == Auth::user()->id)
                            <div class="comment">
                                <h4><span></span> {{ $x->user->name }} <span class="pull-right">
                                {{--<h4><span></span> {{ $x->user_id }} <span class="pull-right">--}}
                                    {{ \Carbon\Carbon::setLocale('nl') }}
                                    {{ \Carbon\Carbon::createFromTimeStamp(strtotime($x->created_at))->diffForHumans() }}
                                </span></h4>
                                {{--<h4><span>Nicolas</span> Vanhecke <span class="pull-right">{{ $x->created_at }}</span></h4>--}}
                                <p>
                                    {{ $x->message }} <br />
                                    <span>
                                        {{ Form::open(array('url' => 'comment/' . $x->id, 'class' => 'pull-right')) }}
                                            {{ Form::hidden('_method', 'DELETE') }}
                                            {{ Form::button('<i class="fa fa-times"></i>', array('type' => 'submit', 'class' => '')) }}
                                        {{ Form::close() }}
                                        <button class="pull-right"><i class="fa fa-pencil"></i></button>
                                    </span>
                                </p>

                            </div>
                        @else
                            <div class="comment">
                                <h4><span></span> {{ $x->user->name }} <span class="pull-right">
{{--                                <h4><span></span> {{ $x->user_id }} <span class="pull-right">--}}
                                    {{ \Carbon\Carbon::setLocale('nl') }}
                                    {{ \Carbon\Carbon::createFromTimeStamp(strtotime($x->created_at))->diffForHumans() }}
                                </span></h4>
                                {{--<h4><span>Nicolas</span> Vanhecke <span class="pull-right">{{ $x->created_at }}</span></h4>--}}
                                <p>{{ $x->message }}</p>
                            </div>
                        @endif
                    @endforeach
                </div>
            @endif

            {{--@if($comments)--}}
                {{--<div id="comment" class="section comments">--}}
                    {{--<h2>Reacties</h2>--}}
                    {{--@foreach($comments as $x)--}}
                        {{--@if(Auth::user() && $x->user_id == Auth::user()->id)--}}
                            {{--<div class="comment">--}}
                                {{--<h4><span></span> {{ $x->user->name }} <span class="pull-right">--}}
                                {{--<h4><span></span> {{ $x->user_id }} <span class="pull-right">--}}
                                    {{--{{ \Carbon\Carbon::setLocale('nl') }}--}}
                                    {{--{{ \Carbon\Carbon::createFromTimeStamp(strtotime($x->created_at))->diffForHumans() }}--}}
                                {{--</span></h4>--}}
                                {{--<h4><span>Nicolas</span> Vanhecke <span class="pull-right">{{ $x->created_at }}</span></h4>--}}
                                {{--<p>--}}
                                    {{--{{ $x->message }} <br />--}}
                                    {{--<span>--}}
                                        {{--{{ Form::open(array('url' => 'comment/' . $x->id, 'class' => 'pull-right')) }}--}}
                                            {{--{{ Form::hidden('_method', 'DELETE') }}--}}
                                            {{--{{ Form::button('<i class="fa fa-times"></i>', array('type' => 'submit', 'class' => '')) }}--}}
                                        {{--{{ Form::close() }}--}}
                                        {{--<button class="pull-right"><i class="fa fa-pencil"></i></button>--}}
                                    {{--</span>--}}
                                {{--</p>--}}

                            {{--</div>--}}
                        {{--@else--}}
                            {{--<div class="comment">--}}
                                {{--<h4><span></span> {{ $x->user->name }} <span class="pull-right">--}}
{{--                                <h4><span></span> {{ $x->user_id }} <span class="pull-right">--}}
                                    {{--{{ \Carbon\Carbon::setLocale('nl') }}--}}
                                    {{--{{ \Carbon\Carbon::createFromTimeStamp(strtotime($x->created_at))->diffForHumans() }}--}}
                                {{--</span></h4>--}}
                                {{--<h4><span>Nicolas</span> Vanhecke <span class="pull-right">{{ $x->created_at }}</span></h4>--}}
                                {{--<p>{{ $x->body }}</p>--}}
                            {{--</div>--}}
                        {{--@endif--}}
                    {{--@endforeach--}}
                {{--</div>--}}
            {{--@endif--}}

            <div id="reageer">
            @if(Auth::user())
                <div class="section commentsform">
                    <div class="form">
                        <h2>Schrijf een reactie</h2>
                        {{ Form::open(array('url' => 'comment')) }}
                        <input type="hidden" value={{Auth::user()->id}} name="userId" />
                        {{--<input type="hidden" value=0 name="userId" />--}}
                        <input type="hidden" value={{ $blog->id }} name="postId" />
                        <div class="form-group">
                            {{ Form::textarea('Bericht',null ,array('class' => 'form-control')) }}
                        </div>
                        {{ Form::submit('Reageer', array('class' => 'btn btn-block btn-red500 pull-right')) }}
                        {{ Form::close() }}
                    </div>
                </div>
            @else
                <div class="section authorize">
                    <h4 onclick="location.href='{{ URL::to('login') }}';" >Log in om een reactie te schrijven</h4>
                </div>
            @endif
            </div>

        </div>
    </div>
</div>

@endsection
