@extends('layouts.404')
@section('title', 'Geen toegang')

@section('content')

<div class="">
    <div class="">
        <div class="col-lg-8 fourofourMessage">
            <h1 class="upc">Tread lightly</h1>
            <h3>Uw account heeft geen toegang tot deze actie</h3>

            <div class="backHome">
                <a class="ghost_btn" href="{{ URL::to('dash') }}">Keer terug naar de homepage</a>
            </div>
        </div>
    </div>
</div>
@endsection
