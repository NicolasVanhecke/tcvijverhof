@extends('layouts.back')
@section('title', 'Blog')

@section('sidemenu') @endsection

{{--   PAGE STRUCTURE | LAYOUTS/INDEX  --}}
{{--<div class="row">--}}
    {{--<div id="sidebar" class="col-lg-1 col-md-1 col-sm-1 hidden-xs"> </div>--}}
    {{--@section('content')--}}
{{--</div>--}}

@section('content')

<div class="row">
    <div id="content" class="col-lg-8 col-lg-offset-3 col-md-8 col-md-offset-3 col-sm-8 col-sm-offset-3 col-xs-10 col-xs-offset-1">
        <div class="row">

            @if (Session::has('message'))
                <div class="alert alert-succes">{{ Session::get('message') }}</div>
            @endif

            @if ($errors->has())
                <div class="alert alert-error">
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br />
                    @endforeach
                </div>
            @endif

            <h2>Back blog</h2>

            <button class="btn btn-block btn-red500" onclick="location.href='{{ URL::to('backend/blog/create') }}';">Nieuwe blogpost</button>

            <table class="datatable table table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Titel</th>
                    <th>Aangemaakt</th>
                    <th>Acties</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($blog as $x)
                    <tr>
                        <td>{{ $x->id }}</td>
                        <td>{{ $x->name }}</td>
                        <td>{{ $x->created_at }}</td>
                        <td>
                            <button class="btn btn-none glyphicon glyphicon-eye-open" type="submit"
                                    onclick="location.href='{{ URL::to('backend/blog/' . $x->id) }}';"></button>

                            <button class="btn btn-none glyphicon glyphicon-pencil" type="submit"
                                    onclick="location.href='{{ URL::to('backend/blog/' . $x->id . '/edit') }}';"></button>

                            {{ Form::open(array('url' => 'backend/blog/' . $x->id, 'class' => 'pull-right')) }}
                            {{ Form::hidden('_method', 'DELETE') }}
                            {{ Form::button('<i></i>', array('type' => 'submit', 'class' => 'btn btn-none btn-del glyphicon glyphicon-remove',
                            'confirmAction' => 'Blogpost verwijderen uit database. Deze actie kan niet ongedaan worden!')) }}
                            {{ Form::close() }}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
</div>

@endsection
