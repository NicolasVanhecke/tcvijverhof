@extends('layouts.back')
@section('title', 'Gebruikers')

@section('sidemenu') @endsection

{{--   PAGE STRUCTURE | LAYOUTS/INDEX  --}}
{{--<div class="row">--}}
{{--<div id="sidebar" class="col-lg-1 col-md-1 col-sm-1 hidden-xs"> </div>--}}
{{--@section('content')--}}
{{--</div>--}}

@section('content')

    <div class="row">
        <div id="content" class="col-lg-8 col-lg-offset-3 col-md-8 col-md-offset-3 col-sm-8 col-sm-offset-3 col-xs-10 col-xs-offset-1">
            <div class="row">

                @if (Session::has('message'))
                    <div class="alert alert-succes">{{ Session::get('message') }}</div>
                @endif

                @if ($errors->has())
                    <div class="alert alert-error">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br />
                        @endforeach
                    </div>
                @endif

                <h2>Back blog detail</h2>

                <div class="section post postback">
                    <h2>{{ $blog->name }}</h2>
                    <p class="pre"><span>{{ $blog->date }}</span> {{ $blog->pre }}</p>
                    @if( $blog->photos )
                        <div class="body">
                            <img class="pull-right" src="{{ $blog->photos }}" alt="" />
                            {!! $blog->body !!}
                        </div>
                    @else
                        <div class="body">{!! $blog->body !!}</div>
                    @endif
                    <div class="after">
                        <p><b>Aangemaakt:</b> {{ $blog->created_at }}
                            <span><b>Gewijzigd:</b> {{ $blog->updated_at }}</span>
                        </p>
                    </div>
                </div>

                <button class="btn btn-pink btn-neutral" onclick="location.href='{{ URL::to('backend/blog/' . $blog->id . '/edit') }}';">Wijzig blogpost</button>
                <button class="btn btn-pink btn-neutral" onclick="location.href='{{ URL::to('backend/blog') }}';">Keer terug</button>

                <div id="comment" class="section comments postback">
                    <h2>Reacties</h2>
                    <table class="datatable table table-hover">
                        <thead>
                        <tr>
                            <th>Aangemaakt</th>
                            <th>Door</th>
                            <th>Reactie</th>
                            <th>Acties</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($commentsnr != 0)
                            @foreach($blog->comments as $x)

                                <tr>
                                    <td>{{ $x->created_at }}</td>
                                    <td>{{ $x->user->name }}</td>
                                    <td>{{ $x->message }}</td>
                                    <td>
                                        {{ Form::open(array('url' => 'comment/' . $x->id, 'class' => 'pull-right')) }}
                                        {{ Form::hidden('_method', 'DELETE') }}
                                        {{ Form::button('<i></i>', array('type' => 'submit', 'class' => 'btn btn-none btn-del glyphicon glyphicon-remove',
                                        'confirmAction' => 'Blogpost verwijderen uit database. Deze actie kan niet ongedaan worden!')) }}
                                        {{ Form::close() }}
                                    </td>
                                </tr>

                            @endforeach
                        @else
                            <tr>
                                <td> Er zijn nog geen reacties geplaatst op deze post </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                            </tr>
                        @endif
                </div>

            </div>
        </div>
    </div>

@endsection


