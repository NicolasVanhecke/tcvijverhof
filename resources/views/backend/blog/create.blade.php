@extends('layouts.back')
@section('title', 'Blog')

@section('sidemenu') @endsection

{{--   PAGE STRUCTURE | LAYOUTS/INDEX  --}}
{{--<div class="row">--}}
{{--<div id="sidebar" class="col-lg-1 col-md-1 col-sm-1 hidden-xs"> </div>--}}
{{--@section('content')--}}
{{--</div>--}}

@section('content')

    <div class="row">
        <div id="content" class="col-lg-8 col-lg-offset-3 col-md-8 col-md-offset-3 col-sm-8 col-sm-offset-3 col-xs-10 col-xs-offset-1">
            <div class="row">

                @if (Session::has('message'))
                    <div class="alert alert-succes">{{ Session::get('message') }}</div>
                @endif

                @if ($errors->has())
                    <div class="alert alert-error">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br />
                        @endforeach
                    </div>
                @endif

                <h2>Back blog create</h2>

                    {{ Form::open(array('url' => 'backend/blog')) }}

                    @if($errors->all())
                        <div class="formErrors">
                            <p>Sommige velden zijn niet correct ingevuld. Controleer nogmaals u formulier.</p>
                        </div>
                    @endif

                    <div class="form-group @if ($errors->has('name')) has-error @endif">
                        {{ Form::label('name', 'Titel') }}
                        {{ Form::text('name',null ,array('class' => 'form-control')) }}
                        @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
                    </div>

                    <div class="form-group @if ($errors->has('pre')) has-error @endif">
                        {{ Form::label('pre', 'Inleiding') }}
                        {{ Form::text('pre',null ,array('class' => 'form-control')) }}
                        @if ($errors->has('pre')) <p class="help-block">{{ $errors->first('pre') }}</p> @endif
                    </div>

                    <div class="form-group @if ($errors->has('body')) has-error @endif">
                        {{ Form::label('body', 'Post') }}
                        {{ Form::textarea('body',null ,array('class' => 'form-control', 'id'=>'editor')) }}
                        @if ($errors->has('body')) <p class="help-block">{{ $errors->first('body') }}</p> @endif
                    </div>

                    <div class="form-group @if ($errors->has('date')) has-error @endif">
                        {{ Form::label('date', 'Datum') }}
                        {{ Form::text('date',null , array('class' => 'form-control')) }}
                        @if ($errors->has('date')) <p class="help-block">{{ $errors->first('date') }}</p> @endif
                    </div>

                    {{ Form::submit('Maak blogpost aan', array('class' => 'btn btn-red500')) }}
                    {{ Form::close() }}

                    <button class="btn btn-neutral" onclick="location.href='{{ URL::to('backend/blog') }}';">Keer terug</button>



            </div>
        </div>
    </div>

@endsection




