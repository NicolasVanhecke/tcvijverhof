@extends('layouts.back')
@section('title', 'Rollen')

@section('sidemenu') @endsection

{{--   PAGE STRUCTURE | LAYOUTS/INDEX  --}}
{{--<div class="row">--}}
{{--<div id="sidebar" class="col-lg-1 col-md-1 col-sm-1 hidden-xs"> </div>--}}
{{--@section('content')--}}
{{--</div>--}}

@section('content')

    <div class="row">
        <div id="content" class="col-lg-8 col-lg-offset-3 col-md-8 col-md-offset-3 col-sm-8 col-sm-offset-3 col-xs-10 col-xs-offset-1">
            <div class="row">

                @if (Session::has('message'))
                    <div class="alert alert-succes">{{ Session::get('message') }}</div>
                @endif

                @if ($errors->has())
                    <div class="alert alert-error">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br />
                        @endforeach
                    </div>
                @endif

                <h2>Back rollen</h2>

                    <table class="table table-detail">
                        <tbody>
                        <tr>
                            <th>#</th>
                            <td>{{ $user->id }}</td>
                        </tr>
                        <tr>
                            <th>Gebruikersnaam</th>
                            <td>{{ $user->name }}</td>
                        </tr>
                        <tr>
                            <th>E-mail</th>
                            <td>{{ $user->email }}</td>
                        </tr>
                        <tr>
                            <th>Naam</th>
                            <td>{{ $user->firstname }} {{ $user->lastname }}</td>
                        </tr>
                        <tr>
                            <th>Admin rol</th>
                            <td>
                                @if($user->isAdmin() == 'true')
                                    {{ Form::model($user, array('route' => array('backend.rollen.update', $user->id), 'method' => 'PUT')) }}
                                    {{ Form::hidden('roleId', '3') }}
                                    {{ Form::hidden('status', 'revoke') }}
                                    {{ Form::submit('Intrekken', array('class' => 'btn btn-xs btn-danger')) }}
                                    {{ Form::close() }}
                                @else
                                    {{ Form::model($user, array('route' => array('backend.rollen.update', $user->id), 'method' => 'PUT')) }}
                                    {{ Form::hidden('roleId', '3') }}
                                    {{ Form::hidden('status', 'grant') }}
                                    {{ Form::submit('Toekennen', array('class' => 'btn btn-xs btn-success')) }}
                                    {{ Form::close() }}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Werknemer rol</th>
                            <td>
                                @if($user->isMedewerker() == 'true')
                                    {{ Form::model($user, array('route' => array('backend.rollen.update', $user->id), 'method' => 'PUT')) }}
                                    {{ Form::hidden('roleId', '1') }}
                                    {{ Form::hidden('status', 'revoke') }}
                                    {{ Form::submit('Intrekken', array('class' => 'btn btn-xs btn-danger')) }}
                                    {{ Form::close() }}
                                @else
                                    {{ Form::model($user, array('route' => array('backend.rollen.update', $user->id), 'method' => 'PUT')) }}
                                    {{ Form::hidden('roleId', '1') }}
                                    {{ Form::hidden('status', 'grant') }}
                                    {{ Form::submit('Toekennen', array('class' => 'btn btn-xs btn-success')) }}
                                    {{ Form::close() }}
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Baas rol</th>
                            <td>
                                @if($user->isBaas() == 'true')
                                    {{ Form::model($user, array('route' => array('backend.rollen.update', $user->id), 'method' => 'PUT')) }}
                                    {{ Form::hidden('roleId', '2') }}
                                    {{ Form::hidden('status', 'revoke') }}
                                    {{ Form::submit('Intrekken', array('class' => 'btn btn-xs btn-danger')) }}
                                    {{ Form::close() }}
                                @else
                                    {{ Form::model($user, array('route' => array('backend.rollen.update', $user->id), 'method' => 'PUT')) }}
                                    {{ Form::hidden('roleId', '2') }}
                                    {{ Form::hidden('status', 'grant') }}
                                    {{ Form::submit('Toekennen', array('class' => 'btn btn-xs btn-success')) }}
                                    {{ Form::close() }}
                                @endif
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <button class="btn btn-neutral" onclick="location.href='{{ URL::to('backend/rollen') }}';">Keer terug</button>

            </div>
        </div>
    </div>

@endsection




