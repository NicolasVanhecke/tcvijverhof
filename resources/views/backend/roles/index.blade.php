@extends('layouts.back')
@section('title', 'Rollen')

@section('sidemenu') @endsection

{{--   PAGE STRUCTURE | LAYOUTS/INDEX  --}}
{{--<div class="row">--}}
    {{--<div id="sidebar" class="col-lg-1 col-md-1 col-sm-1 hidden-xs"> </div>--}}
    {{--@section('content')--}}
{{--</div>--}}

@section('content')

<div class="row">
    <div id="content" class="col-lg-8 col-lg-offset-3 col-md-8 col-md-offset-3 col-sm-8 col-sm-offset-3 col-xs-10 col-xs-offset-1">
        <div class="row">

            @if (Session::has('message'))
                <div class="alert alert-succes">{{ Session::get('message') }}</div>
            @endif

            @if ($errors->has())
                <div class="alert alert-error">
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br />
                    @endforeach
                </div>
            @endif

            <h2>Back rollen</h2>

            <button class="btn btn-block btn-red500" onclick="location.href='{{ URL::to('backend/rollen/create') }}';">Gebruiker nieuwe rol toewijzen</button>

                <h4>Werknemers</h4>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Naam</th>
                        <th>E-mail</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($medewerkers as $m)
                        @foreach($m->users as $u)
                            <tr>
                                <td>{{ $u->id }}</td>
                                <td>{{ $u->name }}</td>
                                <td>{{ $u->email }}</td>
                                <td>
                                    <button class="btn btn-red900" type="submit"
                                            onclick="location.href='{{ URL::to('backend/rollen/' . $u->id . '/edit') }}';">Bewerk rollen</button>
                                </td>
                            </tr>
                        @endforeach
                    @endforeach
                    </tbody>
                </table>

                <h4>Bazen</h4>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Naam</th>
                        <th>E-mail</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($bazen as $b)
                        @foreach($b->users as $u)
                            <tr>
                                <td>{{ $u->id }}</td>
                                <td>{{ $u->name }}</td>
                                <td>{{ $u->email }}</td>
                                <td>
                                    <button class="btn btn-red900" type="submit"
                                            onclick="location.href='{{ URL::to('backend/rollen/' . $u->id . '/edit') }}';">Bewerk rollen</button>
                                </td>
                            </tr>
                        @endforeach
                    @endforeach
                    </tbody>
                </table>


                <h4>Admins</h4>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Naam</th>
                        <th>E-mail</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($admins as $a)
                        @foreach($a->users as $u)
                            <tr>
                                <td>{{ $u->id }}</td>
                                <td>{{ $u->name }}</td>
                                <td>{{ $u->email }}</td>
                                <td>
                                    <button class="btn btn-red900" type="submit"
                                            onclick="location.href='{{ URL::to('backend/rollen/' . $u->id . '/edit') }}';">Bewerk rollen</button>
                                </td>
                            </tr>
                        @endforeach
                    @endforeach
                    </tbody>
                </table>




        </div>
    </div>
</div>

@endsection
