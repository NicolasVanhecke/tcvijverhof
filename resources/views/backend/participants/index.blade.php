@extends('layouts.back')
@section('title', 'Inschrijvingen')

@section('sidemenu') @endsection

{{--   PAGE STRUCTURE | LAYOUTS/INDEX  --}}
{{--<div class="row">--}}
    {{--<div id="sidebar" class="col-lg-1 col-md-1 col-sm-1 hidden-xs"> </div>--}}
    {{--@section('content')--}}
{{--</div>--}}

@section('content')

<div class="row">
    <div id="content" class="col-lg-8 col-lg-offset-3 col-md-8 col-md-offset-3 col-sm-8 col-sm-offset-3 col-xs-10 col-xs-offset-1">
        <div class="row">

            @if (Session::has('message'))
                <div class="alert alert-succes">{{ Session::get('message') }}</div>
            @endif

            @if ($errors->has())
                <div class="alert alert-error">
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br />
                    @endforeach
                </div>
            @endif

            <h2>Back inschrijvingen</h2>

            <h3>Start 2 tennis</h3>
                <table id="datatable" class="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Naam</th>
                        <th>E-mail</th>
                        <th>Niveau</th>
                        <th>Ontvangen</th>
                        <th>Acties</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($start as $x)
                        <tr>
                            <td>{{ $x->id }}</td>
                            <td>{{ $x->name }}</td>
                            <td>{{ $x->email }}</td>
                            <td>
                                @if( $x->niveau== 'start0')
                                    Nooit gespeeld
                                @elseif( $x->niveau== 'start1' )
                                    5 jaar gestopt
                                @else {{-- niveau equals start2 --}}
                                    Beetje ervaring
                                @endif
                            </td>
                            <td>
                                {{ \Carbon\Carbon::setLocale('nl') }}
                                {{ \Carbon\Carbon::createFromTimeStamp(strtotime($x->created_at))->diffForHumans() }}
                            </td>
                            <td>
                                <button class="btn btn-none glyphicon glyphicon-eye-open" type="submit"
                                        onclick="location.href='{{ URL::to('backend/inschrijvingen/' . $x->id) }}';"></button>
                                {{ Form::open(array('url' => 'backend/inschrijvingen/' . $x->id, 'class' => 'pull-right')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                {{ Form::button('<i></i>', array('type' => 'submit', 'class' => 'btn btn-none btn-del glyphicon glyphicon-remove',
                                'confirmAction' => 'Inschrijving verwijderen uit database. Deze actie kan niet ongedaan worden!')) }}
                                {{ Form::close() }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            <h3>Stages</h3>
                <table id="datatable" class="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Naam</th>
                        <th>E-mail</th>
                        <th>Stage</th>
                        <th>Niveau</th>
                        <th>Ontvangen</th>
                        <th>Acties</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($stage as $x)
                        <tr>
                            <td>{{ $x->id }}</td>
                            <td>{{ $x->name }}</td>
                            <td>{{ $x->email }}</td>
                            <td style="text-transform: capitalize">{{ $x->stage }}</td>
                            <td style="text-transform: capitalize">{{ $x->niveau }}</td>
                            <td>
                                {{ \Carbon\Carbon::setLocale('nl') }}
                                {{ \Carbon\Carbon::createFromTimeStamp(strtotime($x->created_at))->diffForHumans() }}
                            </td>
                            <td>
                                <button class="btn btn-none glyphicon glyphicon-eye-open" type="submit"
                                        onclick="location.href='{{ URL::to('backend/inschrijvingen/' . $x->id) }}';"></button>
                                {{ Form::open(array('url' => 'backend/inschrijvingen/' . $x->id, 'class' => 'pull-right')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                {{ Form::button('<i></i>', array('type' => 'submit', 'class' => 'btn btn-none btn-del glyphicon glyphicon-remove',
                                'confirmAction' => 'Inschrijving verwijderen uit database. Deze actie kan niet ongedaan worden!')) }}
                                {{ Form::close() }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            <h3>Evenementen</h3>
                <table id="datatable" class="table table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Naam</th>
                        <th>E-mail</th>
                        <th>Evenement</th>
                        <th>Ontvangen</th>
                        <th>Acties</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($formsevent as $x)
                        <tr>
                            <td>{{ $x->id }}</td>
                            <td>{{ $x->name }}</td>
                            <td>{{ $x->email }}</td>
                            {{--<td>{{ $x->event_id }}</td>--}}
                            <td>{{ $x->event->name }}</td>
                            <td>
                                {{ \Carbon\Carbon::setLocale('nl') }}
                                {{ \Carbon\Carbon::createFromTimeStamp(strtotime($x->created_at))->diffForHumans() }}
                            </td>
                            <td>
                                <button class="btn btn-none glyphicon glyphicon-eye-open" type="submit"
                                        onclick="location.href='{{ URL::to('backend/inschrijvingen/' . $x->id) }}';"></button>
                                {{ Form::open(array('url' => 'backend/inschrijvingen/' . $x->id, 'class' => 'pull-right')) }}
                                {{ Form::hidden('_method', 'DELETE') }}
                                {{ Form::button('<i></i>', array('type' => 'submit', 'class' => 'btn btn-none btn-del glyphicon glyphicon-remove',
                                'confirmAction' => 'Inschrijving verwijderen uit database. Deze actie kan niet ongedaan worden!')) }}
                                {{ Form::close() }}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

        </div>
    </div>
</div>

@endsection
