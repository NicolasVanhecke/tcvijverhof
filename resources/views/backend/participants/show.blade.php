@extends('layouts.back')
@section('title', 'Inschrijvingen')

@section('sidemenu') @endsection

{{--   PAGE STRUCTURE | LAYOUTS/INDEX  --}}
{{--<div class="row">--}}
{{--<div id="sidebar" class="col-lg-1 col-md-1 col-sm-1 hidden-xs"> </div>--}}
{{--@section('content')--}}
{{--</div>--}}

@section('content')

    <div class="row">
        <div id="content" class="col-lg-8 col-lg-offset-3 col-md-8 col-md-offset-3 col-sm-8 col-sm-offset-3 col-xs-10 col-xs-offset-1">
            <div class="row">

                @if (Session::has('message'))
                    <div class="alert alert-succes">{{ Session::get('message') }}</div>
                @endif

                @if ($errors->has())
                    <div class="alert alert-error">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br />
                        @endforeach
                    </div>
                @endif

                <h2>Back inschrijving detail</h2>

                @if( $form->type == 'start' )

                    <table class="table table-detail">
                        <tbody>
                        <tr>
                            <th>#</th>
                            <td>{{ $form->id }}</td>
                        </tr>
                        <tr>
                            <th>Naam</th>
                            <td>{{ $form->name }}</td>
                        </tr>
                        <tr>
                            <th>E-mail</th>
                            <td>{{ $form->email }}</td>
                        </tr>
                        <tr>
                            <th>Niveau</th>
                            <td>
                                @if( $form->niveau == 'start0')
                                    Nooit gespeeld
                                @elseif( $form->niveau == 'start1' )
                                    5 jaar gestopt
                                @else {{-- niveau equals start2 --}}
                                Beetje ervaring
                                @endif
                            </td>
                        </tr>
                        <tr>
                            <th>Ontvangen</th>
                            <td>{{ $form->created_at }}</td>
                        </tr>
                        <tr>
                            <th>Bericht</th>
                            <td>{{ $form->message }}</td>
                        </tr>
                        </tbody>
                    </table>

                @elseif( $form->type == 'stage' )

                    <table class="table table-detail">
                        <tbody>
                        <tr>
                            <th>#</th>
                            <td>{{ $form->id }}</td>
                        </tr>
                        <tr>
                            <th>Naam</th>
                            <td>{{ $form->name }}</td>
                        </tr>
                        <tr>
                            <th>E-mail</th>
                            <td>{{ $form->email }}</td>
                        </tr>
                        <tr>
                            <th>Stage</th>
                            <td>{{ $form->stage }}</td>
                        </tr>
                        <tr>
                            <th>Niveau</th>
                            <td>{{ $form->niveau }}</td>
                        </tr>
                        <tr>
                            <th>Ontvangen</th>
                            <td>{{ $form->created_at }}</td>
                        </tr>
                        <tr>
                            <th>Bericht</th>
                            <td>{{ $form->message }}</td>
                        </tr>
                        </tbody>
                    </table>

                @elseif( $form->type == 'event' )

                    <table class="table table-detail">
                        <tbody>
                        <tr>
                            <th>#</th>
                            <td>{{ $form->id }}</td>
                        </tr>
                        <tr>
                            <th>Naam</th>
                            <td>{{ $form->name }}</td>
                        </tr>
                        <tr>
                            <th>E-mail</th>
                            <td>{{ $form->email }}</td>
                        </tr>
                        <tr>
                            <th>Evenement</th>
                            <td>{{ $form->event_id }}</td>
                        </tr>
                        <tr>
                            <th>Ontvangen</th>
                            <td>{{ $form->created_at }}</td>
                        </tr>
                        <tr>
                            <th>Bericht</th>
                            <td>{{ $form->message }}</td>
                        </tr>
                        </tbody>
                    </table>

                @else

                    <h4 style="text-align: center;">Oeps.. Inschrijving niet gevonden, probeer opnieuw</h4>

                @endif

                <button class="btn btn-neutral" onclick="location.href='{{ URL::to('backend/inschrijvingen') }}';">Keer terug</button>

            </div>
        </div>
    </div>

@endsection


