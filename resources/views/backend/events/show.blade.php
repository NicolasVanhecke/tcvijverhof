@extends('layouts.back')
@section('title', 'Gebruikers')

@section('sidemenu') @endsection

{{--   PAGE STRUCTURE | LAYOUTS/INDEX  --}}
{{--<div class="row">--}}
{{--<div id="sidebar" class="col-lg-1 col-md-1 col-sm-1 hidden-xs"> </div>--}}
{{--@section('content')--}}
{{--</div>--}}

@section('content')

    <div class="row">
        <div id="content" class="col-lg-8 col-lg-offset-3 col-md-8 col-md-offset-3 col-sm-8 col-sm-offset-3 col-xs-10 col-xs-offset-1">
            <div class="row">

                @if (Session::has('message'))
                    <div class="alert alert-succes">{{ Session::get('message') }}</div>
                @endif

                @if ($errors->has())
                    <div class="alert alert-error">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br />
                        @endforeach
                    </div>
                @endif

                <h2>Back evenementen detail</h2>

                <div class="section post postback">
                    <h2>{{ $event->name }}</h2>
                    <p class="pre"><span>{{ $event->date }}</span> {{ $event->pre }}</p>
                    @if( $event->photos )
                        <div class="body">
                            <img class="pull-right" src="{{ $event->photos }}" alt="" />
                            {!! $event->body !!}
                        </div>
                    @else
                        <div class="body">{!! $event->body !!}</div>
                    @endif
                    <div class="after">
                        <p><b>Aangemaakt:</b> {{ $event->created_at }}
                            <span><b>Gewijzigd:</b> {{ $event->updated_at }}</span>
                        </p>
                    </div>
                </div>

                <button class="btn btn-neutral" onclick="location.href='{{ URL::to('backend/evenementen/' . $event->id . '/edit') }}';">Wijzig evenement</button>
                <button class="btn btn-neutral" onclick="location.href='{{ URL::to('backend/evenementen') }}';">Keer terug</button>

                <div class="section events postback">
                    <h2>Reacties</h2>
                    <table class="datatable table table-hover">
                        <thead>
                        <tr>
                            <th>Aangemaakt</th>
                            <th>Door</th>
                            <th>Aantal</th>
                            <th>Bericht</th>
                            <th>Acties</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($inschrijvingsnr != 0)
                            @foreach($event->forms as $x)

                                <tr>
                                    <td>{{ $x->created_at }}</td>
                                    <td>{{ $x->email }}</td>
                                    <td>{{ $x->people }}</td>
                                    <td>{{ $x->message }}</td>
                                    <td>
                                        {{ Form::open(array('url' => 'comment/' . $x->id, 'class' => 'pull-right')) }}
                                        {{ Form::hidden('_method', 'DELETE') }}
                                        {{ Form::button('<i></i>', array('type' => 'submit', 'class' => 'btn btn-none btn-del glyphicon glyphicon-remove',
                                        'confirmAction' => 'Blogpost verwijderen uit database. Deze actie kan niet ongedaan worden!')) }}
                                        {{ Form::close() }}
                                    </td>
                                </tr>

                            @endforeach
                        @else
                            <tr>
                                <td> Er zijn nog geen reacties geplaatst op deze post </td>
                                <td> </td>
                                <td> </td>
                                <td> </td>
                            </tr>
                    @endif
                </div>


                    {{--<table class="table table-detail">--}}
                    {{--<tbody>--}}
                    {{--<tr>--}}
                        {{--<th>#</th>--}}
                        {{--<td>{{ $event->id }}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<th>Titel</th>--}}
                        {{--<td>{{ $event->name }}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<th>Inleiding</th>--}}
                        {{--<td>{{ $event->pre }}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<th>Post</th>--}}
                        {{--<td>{{ $event->body }}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<th>Datum</th>--}}
                        {{--<td>{{ $event->date }}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<th>Prijs</th>--}}
                        {{--<td>{{ $event->price }}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<th>Aangemaakt</th>--}}
                        {{--<td>{{ $event->created_at }}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<th>Laatste wijziging</th>--}}
                        {{--<td>{{ $event->updated_at }}</td>--}}
                    {{--</tr>--}}
                    {{--</tbody>--}}
                {{--</table>--}}

            </div>
        </div>
    </div>

@endsection


