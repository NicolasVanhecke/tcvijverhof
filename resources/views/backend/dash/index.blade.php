@extends('layouts.back')
@section('title', 'Dashboard')

@section('sidemenu') @endsection

{{--   PAGE STRUCTURE | LAYOUTS/INDEX  --}}
{{--<div class="row">--}}
    {{--<div id="sidebar" class="col-lg-1 col-md-1 col-sm-1 hidden-xs"> </div>--}}
    {{--@section('content')--}}
{{--</div>--}}

@section('content')

<div class="row">
    <div id="content" class="col-lg-8 col-lg-offset-3 col-md-8 col-md-offset-3 col-sm-8 col-sm-offset-3 col-xs-10 col-xs-offset-1">
        <div class="row">

            {{--<h2 style="padding-bottom: 10px !important;" >TC Vijverhof backend</h2>--}}
            {{--<h5>Welkom {{ Auth::user()->name }}</h5>--}}
            <h2>Welkom {{ Auth::user()->name }}</h2>

            <div class="col-lg-6 col-md-6 dashsection">
                <h4>Blog <span>{{ $blogcount }}</span></h4>
                @foreach( $lastblogs as $x )
                    <p class="hidden-md hidden-sm hidden-xs" onclick="location.href='{{ URL::to('backend/blog/' . $x->id) }}';">{{ substr($x->name, 0, 45) }} ...</p>
                    <p class="hidden-lg hidden-sm hidden-xs" onclick="location.href='{{ URL::to('backend/blog/' . $x->id) }}';">{{ substr($x->name, 0, 35) }} ...</p>
                    <p class="hidden-lg hidden-md hidden-xs" onclick="location.href='{{ URL::to('backend/blog/' . $x->id) }}';">{{ substr($x->name, 0, 80) }} ...</p>
                    <p class="hidden-lg hidden-md hidden-sm"  onclick="location.href='{{ URL::to('backend/blog/' . $x->id) }}';">{{ substr($x->name, 0, 50) }} ...</p>
                @endforeach
            </div>

            <div class="col-lg-6 col-md-6 dashsection">
                <h4>Evenementen <span>{{ $eventscount }}</span></h4>
                @foreach( $lastevents as $x )
                    <p onclick="location.href='{{ URL::to('backend/evenementen/' . $x->id) }}';">{{ $x->name }}</p>
                @endforeach
            </div>

            <div class="col-lg-12 dashsection">
                <h4>Laatste berichten</h4>
                <table class="table table-hover table-dash">
                    <tbody>
                    @foreach( $lastmsgs as $x )
                        <tr onclick="location.href='{{ URL::to('backend/berichten/' . $x->id) }}';">
                            <td>{{ $x->name }}</td>
                            <td>{{ substr($x->message, 0, 50) }} ...</td>
                            <td>{{ $x->email }}</td>
                            <td>{{ $x->created_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="col-lg-12 dashsection">
                <h4>Nieuwe inschrijvingen</h4>
                <table class="table table-hover table-dash">
                    <tbody>
                    @foreach( $lastparticipants as $x )
                        <tr onclick="location.href='{{ URL::to('backend/inschrijvingen/' . $x->id) }}';">
                            <td>{{ $x->name }}</td>
                            <td>
                                @if( $x->type == 'start' )
                                    Start2tennis
                                @elseif( $x->type == 'event')
                                    Evenement
                                @elseif( $x->type == 'stage')
                                    Stage
                                @else
                                    /
                                @endif
                            </td>
                            <td>{{ $x->message }}</td>
                            <td>{{ $x->created_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            <div class="col-lg-12 dashsection">
                <h4>Nerd stats</h4>
             // Hier komt nog aantal users, aantal comments, aantal visits, blablabla
            </div>

        </div>
    </div>
</div>

@endsection
