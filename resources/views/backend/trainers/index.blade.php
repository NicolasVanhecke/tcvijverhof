@extends('layouts.back')
@section('title', 'Trainers')

@section('sidemenu') @endsection

{{--   PAGE STRUCTURE | LAYOUTS/INDEX  --}}
{{--<div class="row">--}}
    {{--<div id="sidebar" class="col-lg-1 col-md-1 col-sm-1 hidden-xs"> </div>--}}
    {{--@section('content')--}}
{{--</div>--}}

@section('content')

<div class="row">
    <div id="content" class="col-lg-8 col-lg-offset-3 col-md-8 col-md-offset-3 col-sm-8 col-sm-offset-3 col-xs-10 col-xs-offset-1">
        <div class="row">

            @if (Session::has('message'))
                <div class="alert alert-succes">{{ Session::get('message') }}</div>
            @endif

            @if ($errors->has())
                <div class="alert alert-error">
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br />
                    @endforeach
                </div>
            @endif

            <h2>Back trainers</h2>

            <button class="btn btn-block btn-red500" onclick="location.href='{{ URL::to('backend/trainers/create') }}';">Nieuwe trainer</button>

            @foreach( $trainers as $x )
                <div class="trainerback">
                    <img class="col-lg-1 col-md-1" src="{{ asset('assets/photos/'.$x->photo) }}" alt="">
                    <p class="col-lg-3 col-md-3"> {{ $x->firstname }} {{ $x->lastname }} </p>
                    <p class="col-lg-3 col-md-3"> {{ $x->niveau }} </p>
                    <p class="col-lg-3 col-md-3"> {{ $x->cellphone }} </p>
                    <div class="col-lg-2 col-md-2">
                        <button class="btn btn-none glyphicon glyphicon-eye-open" type="submit"
                                onclick="location.href='{{ URL::to('backend/trainers/' . $x->id) }}';"></button>

                        <button class="btn btn-none glyphicon glyphicon-pencil" type="submit"
                                onclick="location.href='{{ URL::to('backend/trainers/' . $x->id . '/edit') }}';"></button>

                        {{ Form::open(array('url' => 'backend/trainers/' . $x->id, 'class' => 'pull-right')) }}
                        {{ Form::hidden('_method', 'DELETE') }}
                        {{ Form::button('<i></i>', array('type' => 'submit', 'class' => 'btn btn-none btn-del glyphicon glyphicon-remove',
                        'confirmAction' => 'Trainer verwijderen uit database. Deze actie kan niet ongedaan worden!')) }}
                        {{ Form::close() }}

                    </div>
                </div>
            @endforeach


        </div>
    </div>
</div>

@endsection
