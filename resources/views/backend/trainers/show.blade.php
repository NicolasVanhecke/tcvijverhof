@extends('layouts.back')
@section('title', 'Trainer')

@section('sidemenu') @endsection

{{--   PAGE STRUCTURE | LAYOUTS/INDEX  --}}
{{--<div class="row">--}}
{{--<div id="sidebar" class="col-lg-1 col-md-1 col-sm-1 hidden-xs"> </div>--}}
{{--@section('content')--}}
{{--</div>--}}

@section('content')

    <div class="row">
        <div id="content" class="col-lg-8 col-lg-offset-3 col-md-8 col-md-offset-3 col-sm-8 col-sm-offset-3 col-xs-10 col-xs-offset-1">
            <div class="row">

                @if (Session::has('message'))
                    <div class="alert alert-succes">{{ Session::get('message') }}</div>
                @endif

                @if ($errors->has())
                    <div class="alert alert-error">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br />
                        @endforeach
                    </div>
                @endif

                <h2>Back trainers detail</h2>

                <div class="col-lg-9 col-md-9">
                    <table class="table table-detail">
                        <tbody>
                        <tr>
                            <th>#</th>
                            <td>{{ $trainer->id }}</td>
                        </tr>
                        <tr>
                            <th>Naam</th>
                            <td>{{ $trainer->firstname }} {{ $trainer->lastname }} </td>
                        </tr>
                        <tr>
                            <th>E-mail</th>
                            <td>{{ $trainer->email }}</td>
                        </tr>
                        <tr>
                            <th>Niveau</th>
                            <td>{{ $trainer->niveau }}</td>
                        </tr>
                        <tr>
                            <th>GSM-nummer</th>
                            <td>{{ $trainer->cellphone }}</td>
                        </tr>
                        <tr>
                            <th>E-mail</th>
                            <td>{{ $trainer->email }}</td>
                        </tr>
                        <tr>
                            <th>Favoriete slag</th>
                            <td>{{ $trainer->stroke }}</td>
                        </tr>
                        <tr>
                            <th>Houdt van</th>
                            <td>{{ $trainer->likes }}</td>
                        </tr>
                        <tr>
                            <th>Extra</th>
                            <td>{{ $trainer->message }}</td>
                        </tr>
                        <tr>
                            <th>Actief sinds</th>
                            <td>{{ $trainer->created_at }}</td>
                        </tr>
                        <tr>
                            <th>Laatste wijziging</th>
                            <td>{{ $trainer->updated_at }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                <img class="col-lg-3 col-md-3" src="{{ asset('assets/photos/'.$trainer->photo) }}" alt="">

            </div>

            <button class="btn btn-neutral" onclick="location.href='{{ URL::to('backend/trainers/' . $trainer->id . '/edit') }}';">Wijzig gebruiker</button>
            <button class="btn btn-neutral" onclick="location.href='{{ URL::to('backend/trainers') }}';">Keer terug</button>

        </div>
    </div>

@endsection


