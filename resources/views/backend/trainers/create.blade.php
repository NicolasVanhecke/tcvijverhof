@extends('layouts.back')
@section('title', 'Trainers')

@section('sidemenu') @endsection

{{--   PAGE STRUCTURE | LAYOUTS/INDEX  --}}
{{--<div class="row">--}}
{{--<div id="sidebar" class="col-lg-1 col-md-1 col-sm-1 hidden-xs"> </div>--}}
{{--@section('content')--}}
{{--</div>--}}

@section('content')

    <div class="row">
        <div id="content" class="col-lg-8 col-lg-offset-3 col-md-8 col-md-offset-3 col-sm-8 col-sm-offset-3 col-xs-10 col-xs-offset-1">
            <div class="row">

                @if (Session::has('message'))
                    <div class="alert alert-succes">{{ Session::get('message') }}</div>
                @endif

                @if ($errors->has())
                    <div class="alert alert-error">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br />
                        @endforeach
                    </div>
                @endif

                <h2>Back trainers create</h2>

                    {{ Form::open(array('url' => 'backend/trainers', 'files' => true)) }}

                    @if($errors->all())
                        <div class="formErrors">
                            <p>Sommige velden zijn niet correct ingevuld. Controleer nogmaals u formulier.</p>
                        </div>
                    @endif

                    <div class="form-group @if ($errors->has('firstname')) has-error @endif">
                        {{ Form::label('firstname', 'Voornaam') }}
                        {{ Form::text('firstname',null ,array('class' => 'form-control')) }}
                        @if ($errors->has('firstname')) <p class="help-block">{{ $errors->first('firstname') }}</p> @endif
                    </div>

                    <div class="form-group @if ($errors->has('lastname')) has-error @endif">
                        {{ Form::label('lastname', 'Achternaam') }}
                        {{ Form::text('lastname',null ,array('class' => 'form-control')) }}
                        @if ($errors->has('lastname')) <p class="help-block">{{ $errors->first('lastname') }}</p> @endif
                    </div>

                    <div class="form-group @if ($errors->has('niveau')) has-error @endif">
                        {{ Form::label('niveau', 'Niveau') }}
                        {{ Form::text('niveau',null ,array('class' => 'form-control')) }}
                        @if ($errors->has('niveau')) <p class="help-block">{{ $errors->first('niveau') }}</p> @endif
                    </div>

                    <div class="form-group @if ($errors->has('email')) has-error @endif">
                        {{ Form::label('email', 'E-mail') }}
                        {{ Form::email('email',null ,array('class' => 'form-control')) }}
                        @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
                    </div>

                    <div class="form-group @if ($errors->has('cellphone')) has-error @endif">
                        {{ Form::label('cellphone', 'GSM nummer') }}
                        {{ Form::text('cellphone',null ,array('class' => 'form-control')) }}
                        @if ($errors->has('cellphone')) <p class="help-block">{{ $errors->first('cellphone') }}</p> @endif
                    </div>

                    <div class="form-group @if ($errors->has('stroke')) has-error @endif">
                        {{ Form::label('stroke', 'Favoriete slag') }}
                        {{ Form::text('stroke',null , array('class' => 'form-control')) }}
                        @if ($errors->has('stroke')) <p class="help-block">{{ $errors->first('stroke') }}</p> @endif
                    </div>

                    <div class="form-group @if ($errors->has('likes')) has-error @endif">
                        {{ Form::label('likes', 'Houdt van') }}
                        {{ Form::text('likes',null , array('class' => 'form-control')) }}
                        @if ($errors->has('likes')) <p class="help-block">{{ $errors->first('likes') }}</p> @endif
                    </div>

                    <div class="form-group @if ($errors->has('message')) has-error @endif">
                        {{ Form::label('message', 'Extra') }}
                        {{ Form::textarea('message',null , array('class' => 'form-control', 'id' => 'editor')) }}
                        @if ($errors->has('message')) <p class="help-block">{{ $errors->first('message') }}</p> @endif
                    </div>

                    <div class="form-group @if ($errors->has('photo')) has-error @endif">
                        {{ Form::label('photo', 'Upload foto (enkel vierkante fotos)', array('class' => '')) }}
                        {{ Form::file('photo', array('class' => 'form-control')) }}
                        @if ($errors->has('photo')) <p class="help-block">{{ $errors->first('photo') }}</p> @endif
                    </div>


                    {{ Form::submit('Maak trainer aan', array('class' => 'btn btn-red500')) }}
                    {{ Form::close() }}

                    <button class="btn btn-neutral" onclick="location.href='{{ URL::to('backend/trainers') }}';">Keer terug</button>



            </div>
        </div>
    </div>

@endsection




