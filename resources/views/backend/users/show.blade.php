@extends('layouts.back')
@section('title', 'Gebruikers')

@section('sidemenu') @endsection

{{--   PAGE STRUCTURE | LAYOUTS/INDEX  --}}
{{--<div class="row">--}}
{{--<div id="sidebar" class="col-lg-1 col-md-1 col-sm-1 hidden-xs"> </div>--}}
{{--@section('content')--}}
{{--</div>--}}

@section('content')

    <div class="row">
        <div id="content" class="col-lg-8 col-lg-offset-3 col-md-8 col-md-offset-3 col-sm-8 col-sm-offset-3 col-xs-10 col-xs-offset-1">
            <div class="row">

                @if (Session::has('message'))
                    <div class="alert alert-succes">{{ Session::get('message') }}</div>
                @endif

                @if ($errors->has())
                    <div class="alert alert-error">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br />
                        @endforeach
                    </div>
                @endif

                <h2>Back gebruikers detail</h2>

                <table class="table table-detail">
                    <tbody>
                    <tr>
                        <th>#</th>
                        <td>{{ $user->id }}</td>
                    </tr>
                    <tr>
                        <th>Gebruikersnaam</th>
                        <td>{{ $user->name }}</td>
                    </tr>
                    {{--<tr>--}}
                        {{--<th>Naam</th>--}}
                        {{--<td>{{ $user->firstname }} {{ $user->lastname }} </td>--}}
                    {{--</tr>--}}
                    <tr>
                        <th>E-mail</th>
                        <td>{{ $user->email }}</td>
                    </tr>
                    {{--<tr>--}}
                        {{--<th>GSM-nummer</th>--}}
                        {{--<td>{{ $user->cellphone }}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<th>Provincie</th>--}}
                        {{--<td>{{ $user->state }}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<th>Club</th>--}}
                        {{--<td>{{ $user->club }}</td>--}}
                    {{--</tr>--}}
                    {{--<tr>--}}
                        {{--<th>Klassement</th>--}}
                        {{--<td>{{ $user->rank }}</td>--}}
                    {{--</tr>--}}
                    <tr>
                        <th>Actief sinds</th>
                        <td>{{ $user->created_at }}</td>
                    </tr>
                    <tr>
                        <th>Laatste wijziging</th>
                        <td>{{ $user->updated_at }}</td>
                    </tr>
                    @foreach ($user->roles as $role)
                        <tr>
                            <th>Rechten als</th>
                            <td>{{ $role->name }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <button class="btn btn-neutral" onclick="location.href='{{ URL::to('backend/gebruikers/' . $user->id . '/edit') }}';">Wijzig gebruiker</button>
                <button class="btn btn-neutral" onclick="location.href='{{ URL::to('backend/gebruikers/') }}';">Keer terug</button>


            </div>
        </div>
    </div>

@endsection


