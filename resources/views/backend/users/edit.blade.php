@extends('layouts.back')
@section('title', 'Gebruikers')

@section('sidemenu') @endsection

{{--   PAGE STRUCTURE | LAYOUTS/INDEX  --}}
{{--<div class="row">--}}
{{--<div id="sidebar" class="col-lg-1 col-md-1 col-sm-1 hidden-xs"> </div>--}}
{{--@section('content')--}}
{{--</div>--}}

@section('content')

    <div class="row">
        <div id="content" class="col-lg-8 col-lg-offset-3 col-md-8 col-md-offset-3 col-sm-8 col-sm-offset-3 col-xs-10 col-xs-offset-1">
            <div class="row">

                @if (Session::has('message'))
                    <div class="alert alert-succes">{{ Session::get('message') }}</div>
                @endif

                @if ($errors->has())
                    <div class="alert alert-error">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br />
                        @endforeach
                    </div>
                @endif

                <h2>Back gebruikers edit</h2>

{{--                    {{ Form::open(array('url' => 'comment')) }}--}}
                    {{ Form::model($user, array('route' => array('backend.gebruikers.update', $user->id), 'method' => 'PUT')) }}

                    @if($errors->all())
                        <div class="formErrors">
                            <p>Sommige velden zijn niet correct ingevuld. Controleer nogmaals u formulier.</p>
                        </div>
                    @endif

                    <div class="form-group @if ($errors->has('name')) has-error @endif">
                        {{ Form::label('name', 'Gebruikersnaam') }}
                        {{ Form::text('name',null ,array('class' => 'form-control')) }}
                        @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
                    </div>

                    {{--<div class="form-group @if ($errors->has('firstname')) has-error @endif">--}}
                        {{--{{ Form::label('firstname', 'Voornaam') }}--}}
                        {{--{{ Form::text('firstname',null ,array('class' => 'form-control')) }}--}}
                        {{--@if ($errors->has('firstname')) <p class="help-block">{{ $errors->first('firstname') }}</p> @endif--}}
                    {{--</div>--}}

                    {{--<div class="form-group @if ($errors->has('lastname')) has-error @endif">--}}
                        {{--{{ Form::label('lastname', 'Achternaam') }}--}}
                        {{--{{ Form::text('lastname',null ,array('class' => 'form-control')) }}--}}
                        {{--@if ($errors->has('lastname')) <p class="help-block">{{ $errors->first('lastname') }}</p> @endif--}}
                    {{--</div>--}}

                    {{--<div class="form-group @if ($errors->has('state')) has-error @endif">--}}
                        {{--{{ Form::label('state', 'Provincie') }}--}}
                        {{--<select id="state" name="state" class="form-control">--}}
                            {{--@foreach ($states as $s)--}}
                                {{--@if($s->name == $userState[0]->state)--}}
                                    {{--<option selected value="{{ $s->name }}">{{ $s->name }}</option>--}}
                                {{--@else--}}
                                    {{--<option value="{{ $s->name }}">{{ $s->name }}</option>--}}
                                {{--@endif--}}
                            {{--@endforeach--}}
                        {{--</select>--}}
                        {{--@if ($errors->has('state')) <p class="help-block">{{ $errors->first('state') }}</p> @endif--}}
                    {{--</div>--}}

                    {{--<div class="form-group @if ($errors->has('cellphone')) has-error @endif">--}}
                        {{--{{ Form::label('cellphone', 'GSM nummer') }}--}}
                        {{--{{ Form::text('cellphone',null ,array('class' => 'form-control')) }}--}}
                        {{--@if ($errors->has('cellphone')) <p class="help-block">{{ $errors->first('cellphone') }}</p> @endif--}}
                    {{--</div>--}}

                    <div class="form-group @if ($errors->has('email')) has-error @endif">
                        {{ Form::label('email', 'Email') }}
                        {{ Form::email('email',null , array('class' => 'form-control')) }}
                        @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
                    </div>

                    {{ Form::submit('Wijzigingen opslaan', array('class' => 'btn btn-red500')) }}

                    {{ Form::close() }}

                    <button class="btn btn-neutral" onclick="location.href='{{ URL::to('backend/gebruikers') }}';">Keer terug</button>


            </div>
        </div>
    </div>

@endsection
