@extends('layouts.back')
@section('title', 'Berichten')

@section('sidemenu') @endsection

{{--   PAGE STRUCTURE | LAYOUTS/INDEX  --}}
{{--<div class="row">--}}
    {{--<div id="sidebar" class="col-lg-1 col-md-1 col-sm-1 hidden-xs"> </div>--}}
    {{--@section('content')--}}
{{--</div>--}}

@section('content')

<div class="row">
    <div id="content" class="col-lg-8 col-lg-offset-3 col-md-8 col-md-offset-3 col-sm-8 col-sm-offset-3 col-xs-10 col-xs-offset-1">
        <div class="row">

            @if (Session::has('message'))
                <div class="alert alert-succes">{{ Session::get('message') }}</div>
            @endif

            @if ($errors->has())
                <div class="alert alert-error">
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br />
                    @endforeach
                </div>
            @endif

            <h2>Back berichten</h2>

            <table class="datatable table table-hover">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Gebruikersnaam</th>
                    <th>E-mail</th>
                    <th>Bericht</th>
                    <th>Ontvangen</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($msg as $x)
                    <tr>
                        <td>{{ $x->id }}</td>
                        <td>{{ $x->name }}</td>
                        <td>{{ $x->email }}</td>
                        <td>{{ substr($x->message, 0, 30) }} ...</td>
                        <td>
                            {{ \Carbon\Carbon::setLocale('nl') }}
                            {{ \Carbon\Carbon::createFromTimeStamp(strtotime($x->created_at))->diffForHumans() }}
                        </td>
                        <td>
                            <button class="btn btn-none glyphicon glyphicon-eye-open" type="submit"
                                    onclick="location.href='{{ URL::to('backend/berichten/' . $x->id) }}';"></button>

                            {{ Form::open(array('url' => 'backend/berichten/' . $x->id, 'class' => 'pull-right')) }}
                            {{ Form::hidden('_method', 'DELETE') }}
                            {{ Form::button('<i></i>', array('type' => 'submit', 'class' => 'btn btn-none btn-del glyphicon glyphicon-remove',
                            'confirmAction' => 'Bericht verwijderen uit database. Deze actie kan niet ongedaan worden!')) }}
                            {{ Form::close() }}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>
    </div>
</div>

@endsection
