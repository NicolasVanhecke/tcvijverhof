@extends('layouts.back')
@section('title', 'Berichten')

@section('sidemenu') @endsection

{{--   PAGE STRUCTURE | LAYOUTS/INDEX  --}}
{{--<div class="row">--}}
{{--<div id="sidebar" class="col-lg-1 col-md-1 col-sm-1 hidden-xs"> </div>--}}
{{--@section('content')--}}
{{--</div>--}}

@section('content')

    <div class="row">
        <div id="content" class="col-lg-8 col-lg-offset-3 col-md-8 col-md-offset-3 col-sm-8 col-sm-offset-3 col-xs-10 col-xs-offset-1">
            <div class="row">

                @if (Session::has('message'))
                    <div class="alert alert-succes">{{ Session::get('message') }}</div>
                @endif

                @if ($errors->has())
                    <div class="alert alert-error">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br />
                        @endforeach
                    </div>
                @endif

                <h2>Back berichten detail</h2>

                <table class="table table-detail">
                    <tbody>
                    <tr>
                        <th>#</th>
                        <td>{{ $msg->id }}</td>
                    </tr>
                    <tr>
                        <th>Naam</th>
                        <td>{{ $msg->name }}</td>
                    </tr>
                    <tr>
                        <th>E-mail</th>
                        <td>{{ $msg->email }}</td>
                    </tr>
                    <tr>
                        <th>Ontvangen</th>
                        <td>{{ $msg->created_at }}</td>
                    </tr>
                    <tr>
                        <th>Bericht</th>
                        <td>{{ $msg->message }}</td>
                    </tr>
                    </tbody>
                </table>

                <button class="btn btn-neutral" onclick="location.href='{{ URL::to('backend/berichten') }}';">Keer terug</button>

            </div>
        </div>
    </div>

@endsection


