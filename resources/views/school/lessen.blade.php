@extends('layouts.index')
@section('title', 'Tennisschool')

@section('sidemenu') @endsection

{{--   PAGE STRUCTURE | LAYOUTS/INDEX  --}}
{{--<div class="row">--}}
{{--<div id="sidebar" class="col-lg-1 col-md-1 col-sm-1 hidden-xs"> </div>--}}
{{--@section('content')--}}
{{--</div>--}}

@section('content')

    <div class="row">
        <div id="content" class="col-lg-8 col-lg-offset-3 col-md-8 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
            <div class="row">

                @if (Session::has('message'))
                    <div class="alert">{{ Session::get('message') }}</div>
                @endif

                <div class="row section lessen">
                    <h2>Lessen</h2>
                    <p>Onze lessenreeksen zijn opgedeeld in twee grote blokken: winterlessen en lentelessen. Het <b>winterseizoen</b> start eind september en verzorgt 20 lessen over een periode van 25 weken. Lessen worden gegeven op onze twee overdekte gravelbanen of in de zaal. Er zijn een aantal open weken om eventuele verloren lessen in te halen.<br />
                        <b>Lentelessen</b> starten na de paasvakantie, of vanaf wanneer het weer het toelaat onze buitenbanen te gebruiken. Een reeks is typisch een 9-tal lessen tot net aan de zomervakantie.</p>
                    <p>Voor een snelle persoonlijke formule kan je terecht bij Christos. Christos is bereikbaar via e-mail op baltogiannischristos@hotmail.com of direct op 0495 53 66 55.</p>
                </div>

            </div>
        </div>
    </div>

@endsection
