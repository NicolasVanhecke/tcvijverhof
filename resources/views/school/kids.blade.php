@extends('layouts.index')
@section('title', 'Tennisschool')

@section('sidemenu') @endsection

{{--   PAGE STRUCTURE | LAYOUTS/INDEX  --}}
{{--<div class="row">--}}
{{--<div id="sidebar" class="col-lg-1 col-md-1 col-sm-1 hidden-xs"> </div>--}}
{{--@section('content')--}}
{{--</div>--}}

@section('content')

    <div class="row">
        <div id="content" class="col-lg-8 col-lg-offset-3 col-md-8 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
            <div class="row">

                @if (Session::has('message'))
                    <div class="alert">{{ Session::get('message') }}</div>
                @endif

                    <div class="row section kidstennis">
                        <h2>Kidstennis</h2>
                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12" style="padding-left:0;">
                            <p>De opleiding is opgedeeld in 5 vaardigheidsniveaus die telkens een andere kleur hebben. Kinderen leren stap voor stap tennissen. Vanaf 3 jaar is uw kleine spruit welkom bij ons. Wij zorgen voor aangepaste rackets, tennisballen en oefeningen per reeks. Dit kun je als ouder allemaal volgend in het grote tennisboek. Aan de hand van testjes en diploma's blijft iedereen gemotiveerd om telkens je beste beentje voor te zetten.
                                De vereisten van de 5 KidsTenniskleuren staan uitvoerig omschreven in het Trainersboek en zorgen ervoor dat er naar een gericht doel kan gewerkt worden.</p>
                            <p><a href="http://www.tennisvlaanderen.be/kidstennis">Dit alles komt uitgebreid aan bod op de site van tennisvlaanderen. Lees hier verder.</a></p>
                        </div>
                        <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">
                            <img class="pull-right" src="{{ asset('assets/images/kidstennis.gif') }}" alt="">
                        </div>
                    </div>

            </div>
        </div>
    </div>

@endsection
