@extends('layouts.index')
@section('title', 'Tennisschool')

@section('sidemenu') @endsection

{{--   PAGE STRUCTURE | LAYOUTS/INDEX  --}}
{{--<div class="row">--}}
{{--<div id="sidebar" class="col-lg-1 col-md-1 col-sm-1 hidden-xs"> </div>--}}
{{--@section('content')--}}
{{--</div>--}}

@section('content')

    <div class="row">
        <div id="content" class="col-lg-8 col-lg-offset-3 col-md-8 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
            <div class="row">

                @if (Session::has('message'))
                    <div class="alert">{{ Session::get('message') }}</div>
                @endif

                <div class="row section schoolNav">
                    <h2>Ik ben op zoek naar</h2>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="location.href='{{ URL::to('school/start2tennis') }}';">
                        <div class="block">Start to tennis</div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="location.href='{{ URL::to('school/kids') }}';">
                        <div class="block">Kids tennis</div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="location.href='{{ URL::to('school/stages') }}';">
                        <div class="block">Stages</div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" onclick="location.href='{{ URL::to('school/lessen') }}';">
                        <div class="block">Lessen op maat</div>
                    </div>
                </div>

                <div class="row section team">
                    <h2>Ontmoet ons team</h2>
                        @foreach( $trainers as $x )
                            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 trainer">
                                @if( $x->photo )
                                    <img class="img-responsive trainerimg" src="{{ asset('assets/photos/'.$x->photo) }}" alt="trainer"
                                         onclick="location.href='{{ URL::to('trainers/' . $x->id) }}'" />
                                @else
                                    <img class="img-responsive trainerimg" src="{{ asset('assets/photos/dummy.jpg') }}" alt="trainer"
                                         onclick="location.href='{{ URL::to('trainers/' . $x->id) }}'" />
                                @endif
                                <span>{{ $x->firstname }} <b>{{ $x->lastname }}</b></span>
                                <span>{{ $x->niveau }}</span>
                            </div>
                        @endforeach
                </div>

            </div>
        </div>
    </div>

@endsection
