@extends('layouts.index')
@section('title', 'Tennisschool')

@section('sidemenu') @endsection

{{--   PAGE STRUCTURE | LAYOUTS/INDEX  --}}
{{--<div class="row">--}}
{{--<div id="sidebar" class="col-lg-1 col-md-1 col-sm-1 hidden-xs"> </div>--}}
{{--@section('content')--}}
{{--</div>--}}

@section('content')

    <div class="row">
        <div id="content" class="col-lg-8 col-lg-offset-3 col-md-8 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
            <div class="row">

                @if (Session::has('message'))
                    <div class="alert alert-succes">{{ Session::get('message') }}</div>
                @endif

                @if ($errors->has())
                    <div class="alert alert-error">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br />
                        @endforeach
                    </div>
                @endif

                    <div class="row section stages">
                        <h2>Stages</h2>
                        <div class="col-lg-12" style="padding-left:0;">
                            <p>Zowel in de paasvakantie als tijdens de zomervakantie organiseert de club tennisstages. Wij hebben twee formules: kids- en omisportstages en competitiestages. Voor de iets oudere is er een zeer uitdagende competitiestage. Hierin heb je de mogelijkheid om met je vrienden en coach mee te gaan naar een tennistoernooi. Vervoer naar en van de toernooien wordt overlegd met de ouders.
                                Kampen beginnen om 9u en eindigen om 16u. Er is een opvang voorzien vanaf 8u en op het einde van de dag tot 17u. Ieder kind brengt zijn eigen lunch mee voor tijdens de middagpauze.
                            </p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 stage" style="padding-left:0;">
                            <img class="square" src="{{ asset('assets/images/stageomni.png') }}" alt="">
                        </div>
                        <div class="hidden-lg hidden-md hidden-sm col-xs-12 stage" style="padding-left:0;">
                            <h3>Kids- en omnisportstage</h3>
                            <p>Tennis met afwisseling van omnisporten. Iedeaal voor de kleinsten en kinderen tot 8 jaar.</p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 stage" style="padding-left:0;">
                            <img class="square" src="{{ asset('assets/images/stagecomp.png') }}" alt="">
                        </div>
                        <div class="hidden-lg hidden-md hidden-sm col-xs-12 stage" style="padding-left:0;">
                            <h3>Competitiestage</h3>
                            <p>Competitietraining voor gemotiveerde spelers vanaf 8 jaar. Met mogelijkheid op tornooibegeleiding door coaches.</p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs stage" style="padding-left:0;">
                            <h3>Kids- en omnisportstage</h3>
                            <p>Tennis met afwisseling van omnisporten. Iedeaal voor de kleinsten en kinderen tot 8 jaar.</p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 hidden-xs stage" style="padding-left:0;">
                            <h3>Competitiestage</h3>
                            <p>Competitietraining voor gemotiveerde spelers vanaf 8 jaar. Met mogelijkheid op tornooibegeleiding door coaches.</p>
                        </div>
                    </div>

                    <div class="row section">
                        <h2>Schrijf je hier in</h2>
                        {{ Form::open(array('url' => 'form')) }}
                        <input type="hidden" value="stage" name="Type" />
                        <div class="form-group">
                            {{ Form::label('Naam', 'Naam') }}
                            {{ Form::text('Naam',null ,array('class' => 'form-control')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('Email', 'Email') }}
                            {{ Form::text('Email',null ,array('class' => 'form-control')) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('Stage', 'Stage') }}
                            {{ Form::select('Stage', array('kids' => 'Kids- en omnisportstage', 'competitie' => 'Competitiestage'), 'kids', ['class' => 'form-control']) }}
                            {{--{{ Form::text('Niveau',null ,array('class' => 'form-control')) }}--}}
                        </div>
                        <div class="form-group">
                            {{ Form::label('Niveau', 'Niveau') }}
                            {{ Form::select('Niveau', array('wit' => 'Wit', 'rood' => 'Rood', 'groen' => 'Groen', 'blauw' => 'Blauw', 'oranje' => 'Oranje'), 'wit', ['class' => 'form-control']) }}
                            {{--{{ Form::text('Niveau',null ,array('class' => 'form-control')) }}--}}
                        </div>
                        <div class="form-group">
                            {{ Form::label('Bericht', 'Extra vermelding of uitleg') }}
                            {{ Form::textarea('Bericht',null ,array('class' => 'form-control')) }}
                        </div>
                        {{ Form::submit('Verstuur', array('class' => 'btn btn-block btn-red500 pull-right')) }}
                        {{ Form::close() }}
                    </div>

            </div>
        </div>
    </div>

@endsection
