@extends('layouts.index')
@section('title', 'Tennisschool')

@section('sidemenu') @endsection

{{--   PAGE STRUCTURE | LAYOUTS/INDEX  --}}
{{--<div class="row">--}}
{{--<div id="sidebar" class="col-lg-1 col-md-1 col-sm-1 hidden-xs"> </div>--}}
{{--@section('content')--}}
{{--</div>--}}

@section('content')

    <div class="row">
        <div id="content" class="col-lg-8 col-lg-offset-3 col-md-8 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
            <div class="row">

                @if (Session::has('message'))
                    <div class="alert alert-succes">{{ Session::get('message') }}</div>
                @endif

                @if ($errors->has())
                    <div class="alert alert-error">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br />
                        @endforeach
                    </div>
                @endif

                <div class="row section start2tennis">
                    <h2>Start 2 tennis</h2>
                    <p>Start to Tennis is een voordelig en compact lessenpakket voor volwassenen, met een lessenreeks, aangepast materiaal en plezierige speelmomenten. Met de vernieuwde tennisaanpak heb jij ook binnen 10 uur het spel onder de knie, wat je leeftijd of niveau ook is. Leren tennissen was nooit zo gemakkelijk!</p>
                    <div class="subsection col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <img class="img-responsive center-block" src="{{ asset('assets/icons/calendar.png') }}" alt="">
                        <p>Je krijgt <strong>7 lessen</strong> van anderhalve uur. Jij <strong>kiest zelf wanneer</strong>!</p>
                    </div>
                    <div class="subsection col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <img class="img-responsive center-block" src="{{ asset('assets/icons/ball.png') }}" alt="">
                        <p>Word je nadien bij ons lid? Dan krijg je <strong>gratis</strong> een <strong>nieuwe Prince racket</strong>.</p>
                    </div>
                    <div class="subsection col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <img class="img-responsive center-block" src="{{ asset('assets/icons/bag.png') }}" alt="">
                        <p>Je betaald <strong>€80</strong> voor 7 lessen. Nadien word je <strong>goedkoper lid</strong>.</p>
                    </div>
                </div>

                <div class="row section">
                    <h2>Schrijf je hier in</h2>
                    {{ Form::open(array('url' => 'form')) }}
                    <input type="hidden" value="start" name="Type" />
                    <div class="form-group">
                        {{ Form::label('Naam', 'Naam') }}
                        {{ Form::text('Naam',null ,array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('Email', 'Email') }}
                        {{ Form::text('Email',null ,array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('Niveau', 'Niveau') }}
                        {{ Form::select('Niveau', array('start0' => 'Ik heb nog nooit les gekregen', 'start1' => 'Ik speel al meer dan 5 jaar niet', 'start2' => 'Ik weet wat ik doe, maar ook niet altijd'), 'start0', ['class' => 'form-control']) }}
                        {{--{{ Form::text('Niveau',null ,array('class' => 'form-control')) }}--}}
                    </div>
                    <div class="form-group">
                        {{ Form::label('Bericht', 'Extra vermelding of uitleg') }}
                        {{ Form::textarea('Bericht',null ,array('class' => 'form-control', 'placeholder' => 'Voorkeur welke dagen in de week, met welke groepsleden')) }}
                    </div>
                    {{ Form::submit('Verstuur', array('class' => 'btn btn-block btn-red500 pull-right')) }}
                    {{ Form::close() }}
                </div>

            </div>
        </div>
    </div>

@endsection
