@extends('layouts.index')
@section('title', 'Contact')

@section('sidemenu') @endsection

{{--   PAGE STRUCTURE | LAYOUTS/INDEX  --}}
{{--<div class="row">--}}
{{--<div id="sidebar" class="col-lg-1 col-md-1 col-sm-1 hidden-xs"> </div>--}}
{{--@section('content')--}}
{{--</div>--}}

@section('content')

    <div class="row">
        <div id="content" class="col-lg-8 col-lg-offset-3 col-md-8 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
            <div class="row">

                @if (Session::has('message'))
                    <div class="alert alert-succes">{{ Session::get('message') }}</div>
                @endif

                @if ($errors->has())
                    <div class="alert alert-error">
                        @foreach ($errors->all() as $error)
                            {{ $error }}<br />
                        @endforeach
                    </div>
                @endif

                <div class="section contact">
                    <h2>Hoe bereik je ons</h2>
                    <div class="text col-lg-4 col-lg-offset-0 col-md-4 col-md-offset-0 col-sm-12 col-sm-offset-0 col-xs-12col-xs-offset-0 ">
                        <p>Vul onderstaand formulier in en wij contacteren u zo snel mogelijk. Voor dringende zaken kunt u ons steeds bereiking via telefoon</p>
                        <p><span>Christos </span>0488 746 574</p>
                        <p><span>Christophe </span>0493 226 363</p>
                        <p><span>Club </span>050 84 09 09</p>
                        <p>Antwerpse Heirweg 22A <br /> 8340 Damme-Sijsele</p>
                    </div>

                    <div class="contactform col-lg-7 col-lg-offset-1 col-md-7 col-md-offset-1 col-sm-12 col-offset-sm-0 col-xs-12 col-xs-offset-0">
                        {{ Form::open(array('url' => 'form')) }}
                            <input type="hidden" value="contact" name="Type" />
                            {{--<input type="hidden" value=null name="Eventid" />--}}
                            {{--<input type="hidden" value=null name="Stage" />--}}
                            {{--<input type="hidden" value=null name="Niveau" />--}}
                            {{--<input type="hidden" value=null name="People" />--}}
                            <div class="form-group">
                                {{ Form::label('Naam', 'Naam') }}
                                {{ Form::text('Naam',null ,array('class' => 'form-control')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('Email', 'Email') }}
                                {{ Form::text('Email',null ,array('class' => 'form-control')) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('Bericht', 'Bericht') }}
                                {{ Form::textarea('Bericht',null ,array('class' => 'form-control')) }}
                            </div>
                            {{ Form::submit('Verstuur', array('class' => 'btn btn-block btn-red500 pull-right')) }}
                        {{ Form::close() }}
                    </div>

                </div>

                <div class="section gmaps">
                    <div id="map"></div>
                </div>

            </div>
        </div>
    </div>

@endsection

{{--<form id="form">--}}
{{--<h1>Material Design Contact Form with Validation</h1>--}}
{{--<input placeholder="Name" type="text"  value="" required>--}}
{{--<input placeholder="Email address" type="email" onblur="this.setAttribute('value', this.value);" value="" required>--}}
{{--<span class="validation-text">Please enter a valid email address.</span>--}}
{{--<input placeholder="Location" type="text" value="" required>--}}
{{--<div class="flex">--}}
{{--<textarea placeholder="Message" rows="1" required></textarea>--}}
{{--</div>--}}
{{--<button>Send</button>--}}
{{--</form>--}}

