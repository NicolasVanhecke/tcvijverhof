<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

    <title>TC Vijverhof</title>

    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,300,700' rel='stylesheet' type='text/css'>

    <!-- Styles -->
    {{--<link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet">--}}
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Play' rel='stylesheet' type='text/css'>
    <link href="{{ asset('assets/css/app.css') }}" rel="stylesheet">

    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
    <script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js"></script>
        <script src="{{ asset('assets/js/app.js') }}"></script>
</head>

<body>
<div class="bgHome"></div>
{{--<div class="container">--}}
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                {{--<a class="navbar-brand" href="#">Laravel</a>--}}
            </div>

            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    @if (Auth::guest())
                        <li><a style="color:#fff;" href="{{ url('blog') }}">Blog</a></li>
                        <li><a style="color:#fff;" href="{{ url('evenementen') }}">Evenementen</a></li>
                        <li><a style="color:#fff;" href="{{ url('tennisschool') }}">Tennisschool</a></li>
                        <li><a style="color:#fff;" href="{{ url('club') }}">Club</a></li>
                        <li><a style="color:#fff;" href="{{ url('contact') }}">Contact</a></li>
                        <li><a style="color:#fff;" href="{{ url('/login') }}">Login</a></li>
                    @else
                        <li><a style="color:#fff;" href="{{ url('blog') }}">Blog</a></li>
                        <li><a style="color:#fff;" href="{{ url('evenementen') }}">Evenementen</a></li>
                        <li><a style="color:#fff;" href="{{ url('tennisschool') }}">Tennisschool</a></li>
                        <li><a style="color:#fff;" href="{{ url('club') }}">Club</a></li>
                        <li><a style="color:#fff;" href="http://www.tennisvlaanderen.be/home?p_p_state=maximized&p_p_mode=view&saveLastPath=0&_58_struts_action=%2Flogin%2Flogin&p_p_id=58&p_p_lifecycle=0&_58_redirect=%2Freserveer-een-terrein">Terreinreservatie</a></li>
                        <li><a style="color:#fff;" href="{{ url('contact') }}">Contact</a></li>
                        <li><a style="color:#fff;" href="{{ url('/logout') }}">Logout</a></li>
                    @endif
                </ul>

            </div>
        </div>
    </nav>
{{--</div>--}}

    <div class="logoH">
        <h1>TC Vijverhof</h1>
    </div>

    <div id="ghost" class="hidden-lg hidden-md">
        <div class="eventsH ghostbtn" onclick="location.href=('{{ url('evenementen') }}')"> Evenementen </div>
        <div class="blogH ghostbtn" onclick="location.href=('{{ url('blog') }}')"> Blog </div>
    </div>

    <div id="largeH" class="hidden-sm hidden-xs">
        @foreach($event as $x)
            <div class="col-lg-4 col-lg-offset-8 col-md-5 col-md-offset-7 largeH">
                <h4>{{ $x->name }}</h4>
                <p><span>{{ $x->date }}</span> {{ $x->pre }}</p>
                    <div class="more">
                        <span class="pull-right">
                            <a href="{{ URL::to('/evenementen/' . $x->id . '#inschrijven') }}">Schrijf je in</a>
                        </span>
                        <span class="pull-right">
                            <a href="{{ URL::to('/evenementen/' . $x->id) }}">Lees verder</a>
                        </span>
                    </div>
            </div>
        @endforeach
    </div>

    {{--<div class="row variableSection">--}}
        {{--<div class="col-lg-12 variable">--}}
            {{--<div id="geenLidContent" class="col-lg-12 club">--}}
                {{--<h3>TCV - We are tennis</h3>--}}
                {{--<p>TC Vijverhof is meer dan alleen tennis. Wij streven ernaar om leden (en niet leden) steeds met een warm hart te ontvangen in onze club. Daarom organiseren wij ook tal van activiteiten om de vriendengroep van TC Vijverhof samen te brengen voor leuke avonden of namiddagen.<br />--}}
                    {{--Niet leden zijn ook vaak welkom, maar voor onze leden gaan we net dat stapje verder.<br />--}}
                    {{--Aarzel dus niet en bekijk al onze <a href="{{ URL::to('evenementen') }}">evenementen</a> of <a href="{{ URL::to('contact') }}">contacteer</a> ons voor een inschrijving of eerste tennisles.</p>--}}

            {{--</div>--}}
            {{--<div id="lidContent">--}}
                {{--@foreach($event as $x)--}}
                    {{--<div class="col-lg-12 event">--}}
                        {{--<h4>{{ $x->name }}</h4>--}}
                        {{--<p><span>{{ $x->date }}</span> {{ $x->pre }}</p>--}}
                        {{--<div class="more">--}}
                            {{--<span class="pull-right">--}}
                                {{--<a href="{{ URL::to('/evenementen/' . $x->id . '#inschrijven') }}">Schrijf je in</a>--}}
                            {{--</span>--}}
                            {{--<span class="pull-right">--}}
                                {{--<a href="{{ URL::to('/evenementen/' . $x->id) }}">Lees verder</a>--}}
                            {{--</span>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--@endforeach--}}
            {{--</div>--}}
        {{--</div>--}}

        {{--<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 buttons">--}}
            {{--<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">--}}
                {{--<div id="lid" class="ghostbtn">--}}
                    {{--Ik ben reeds lid--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">--}}
                {{--<div id="geenLid" class="ghostbtn">--}}
                    {{--Ik wil lid worden--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}

    {{--</div>--}}

</body>
</html>

