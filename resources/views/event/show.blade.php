@extends('layouts.index')
@section('title', 'Blog')

@section('sidemenu') @endsection

{{--   PAGE STRUCTURE | LAYOUTS/INDEX  --}}
{{--<div class="row">--}}
    {{--<div id="sidebar" class="col-lg-1 col-md-1 col-sm-1 hidden-xs"> </div>--}}
    {{--@section('content')--}}
{{--</div>--}}

@section('content')

<div class="row">
    <div id="content" class="col-lg-8 col-lg-offset-3 col-md-8 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
        <div class="row">

            @if (Session::has('message'))
                <div class="alert alert-succes">{{ Session::get('message') }}</div>
            @endif

            @if ($errors->has())
                <div class="alert alert-error">
                    @foreach ($errors->all() as $error)
                        {{ $error }}<br />
                    @endforeach
                </div>
            @endif

                <div class="section event">
                    <h2>{{ $event->name }}</h2>
                    <p class="pre"><span>{{ $event->date }}</span> {{ $event->pre }}</p>
                    {{--<p>{{ html_entity_decode(stripslashes($x->body)) }}</p>--}}
                    @if( $event->photos )
                        <p class="body">
                            <img class="pull-right" src="{{ $event->photos }}" alt="" />
                            {!! $event->body !!}
                        </p>
                    @else
                        <p>{!! $event->body !!}</p>
                    @endif
                </div>

                <div id="inschrijven" class="row section">
                    <h2>Schrijf je hier in</h2>
                    {{ Form::open(array('url' => 'form')) }}
                    <input type="hidden" value="event" name="Type" />
                    <input type="hidden" value={{ $event->id }} name="Eventid" />
                    <div class="form-group">
                        {{ Form::label('Naam', 'Naam') }}
                        {{ Form::text('Naam',null ,array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('Email', 'Email') }}
                        {{ Form::text('Email',null ,array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('Personen', 'Aantal personen') }}
                        {{ Form::selectRange('Personen', 1, 5, 1, array('class' => 'form-control')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('Bericht', 'Extra vermelding of uitleg') }}
                        {{ Form::textarea('Bericht',null ,array('class' => 'form-control')) }}
                    </div>
                    {{ Form::submit('Schrijf in', array('class' => 'btn btn-block btn-red500 pull-right')) }}
                    {{ Form::close() }}
                </div>


        </div>
    </div>
</div>

@endsection
