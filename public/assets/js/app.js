/**
 * Created by Nicolas on 07/11/15.
 */

$(document).ready(function() {
    console.log("started");
    $(".alert-succes").delay(3500).fadeOut(1000);
    $(".alert-error").delay(3500);
    bootstrapDataTable();
    confirmDelete();
    loadCKEditor();
    positionHomeGhostButtons();
    positionHomeEvents();
    positionSidenavVertically();
    activeClassMenu();
    authPositioning();
    squareTrainers();
    squareUnit();
    gMaps();
    console.log("all functions loaded");
});

// reload functions on window resize
$(window).resize(function() {
    positionHomeGhostButtons();
    positionHomeEvents();
    positionSidenavVertically();
    authPositioning();
    squareTrainers();
    squareUnit();
});

function bootstrapDataTable(){
    $('.datatable').DataTable({
        "pageLength" : 25
    });
}

function confirmDelete(){
    $("[confirmAction]").click(function(e){
        var $el = $(this);
        e.preventDefault();
        var confirmText = $el.attr('confirmAction');
        bootbox.confirm(confirmText, function(result) {
            if (result) {
                $el.closest('form').submit();
            }
        });
    });
}

function loadCKEditor(){
    // check if there is an editor present
    var editorcheck = document.getElementById('editor');

    if (editorcheck != null) {
        // load editor by id
        CKEDITOR.replace( 'editor' );
    }
}

function positionHomeEvents(){
    // get viewport height and nav height
    var vpH = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    var largeH = $(".largeH").height();

    // calculate middle and add for margin
    var margin = (vpH - (3 * largeH)) - 150;
    styles = {'margin-top': (margin+'px') };
    $("#largeH").css(styles);
}

function positionHomeGhostButtons(){
    // get viewport height and nav height
    var vpH = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    var ghostH = $("#ghost").height();

    // calculate middle and add for margin
    var margin = (vpH - (2 * ghostH)) - 100;
    styles = {'margin-top': (margin+'px') };
    $("#ghost").css(styles);
}

function positionSidenavVertically(){
    // get viewport height and nav height
    var vpH = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    var navH = $("#nav").height();

    // calculate middle and add for margin
    var margin = (vpH - navH)/2;
    styles = {'margin-top': (margin+'px') };
    $("#nav").css(styles);
}

function activeClassMenu(){
    $('#nav ul li a').click(function(){
        $('li a').removeClass("active");
        $(this).addClass("active");
    });}

function squareTrainers(){
    var width = $('.trainerimg').width();

    styles = {'height': (width+'px') };
    $(".trainerimg").css(styles);
}

function authPositioning(){
    var vpH = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    var height = $('.authBlock').width();

    var position = ( ( vpH - height ) / 2 ) - 100;

    styles = {'margin-top': (position+'px') };
    $(".authBlock").css(styles);
}

function squareUnit(){
    var width = $('.square').width();

    styles = {'height': (width+'px') };
    $(".square").css(styles);
}

function gMaps(){
    if(document.getElementById("map") != null){
        console.log('Google Maps loading');
        var clubLatLng = {lat: 51.2113815, lng: 3.3402804};

        var map;
        map = new google.maps.Map(document.getElementById('map'), {
            center: clubLatLng,
            zoom: 13
        });

        var marker = new google.maps.Marker({
            position: clubLatLng,
            map: map
        });
    } else {
        console.log('No Google Maps detected on current page');
    }
}
