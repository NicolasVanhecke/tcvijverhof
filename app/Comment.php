<?php

namespace App;

//use Illuminate\Foundation\Auth\User as Authenticatable;
//use Spatie\Permission\Traits\HasRoles;

use Illuminate\Database\Eloquent\Model;

//class Comment extends Authenticatable
class Comment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'blog_id', 'message'
    ];

    /**
     * Comment belongs to only one ****
     */
    public function blog()
    {
        return $this->belongsTo('App\Blog');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
