<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'pre', 'body', 'date', 'price', 'afterword',
    ];

    /**
     * Event has many forms(enrolments)
     */
    public function forms()
    {
        return $this->hasMany('App\Form');
    }

}
