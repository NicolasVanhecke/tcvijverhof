<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description'
    ];

    /**
     * Relations
     *
     * Many-to-many relation with App\User
     *
     */
    public function users()
    {
        return $this->belongsToMany('App\User', 'users_has_roles');
    }
}
