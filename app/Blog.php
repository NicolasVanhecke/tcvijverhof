<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'pre', 'body', 'date',
    ];

    /**
     * Blog has many comments
     */
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }
}
