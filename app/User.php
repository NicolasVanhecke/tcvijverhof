<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Relations
     *
     * Many-to-many relation with App\Role
     *
     */
    public function roles()
    {
        return $this->belongsToMany('App\Role', 'users_has_roles');
    }

    /**
     * Blog has many comments
     */
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }


    /**
     * Function to check if user has specific role
     *
     * @return bool
     */
    public function isAdmin()
    {
        foreach ($this->roles()->get() as $role)
        {
            if ($role->name == 'admin')
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Function to check if user has specific role
     *
     * @return bool
     */
    public function isMedewerker()
    {
        foreach ($this->roles()->get() as $role)
        {
            if ($role->name == 'medewerker')
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Function to check if user has specific role
     *
     * @return bool
     */
    public function isBaas()
    {
        foreach ($this->roles()->get() as $role)
        {
            if ($role->name == 'baas')
            {
                return true;
            }
        }
        return false;
    }

}
