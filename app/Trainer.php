<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trainer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'niveau', 'photo', 'cellphone', 'email', 'stroke', 'likes', 'message',
    ];
}
