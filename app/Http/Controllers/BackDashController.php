<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Comment;
use App\Event;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class BackDashController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->isMedewerker() or Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            $blogcount = Blog::all()->count();
            $eventscount = Event::all()->count();

            $lastblogs = DB::table('blogs')->orderBy('created_at', 'DESC')->take(3)->get();
            $lastevents = DB::table('events')->orderBy('created_at', 'DESC')->take(3)->get();
            $lastmsgs = DB::table('forms')->where('type', '=', 'contact')->orderBy('created_at', 'DESC')->take(5)->get();
            $lastparticipants = DB::table('forms')->where('type', '!=', 'contact')->orderBy('created_at', 'DESC')->take(5)->get();

            return view('backend.dash.index',[
                'blogcount'         => $blogcount,
                'eventscount'       => $eventscount,
                'lastblogs'         => $lastblogs,
                'lastevents'        => $lastevents,
                'lastmsgs'          => $lastmsgs,
                'lastparticipants'  => $lastparticipants,
            ]);
        } else{ return Redirect::to('permission'); }
    }

}
