<?php

namespace App\Http\Controllers;

use App\Form;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'Naam'      => 'required|min:3',
            'Email'     => 'required|email',
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()
                ->withErrors($validator)
                ->withInput();
        } else {
            $form = new Form();
                $form->name         = Input::get('Naam');
                $form->email        = Input::get('Email');
                $form->type         = Input::get('Type');
                $form->event_id     = Input::get('Eventid');
                $form->stage        = Input::get('Stage');
                $form->niveau       = Input::get('Niveau');
                $form->people       = Input::get('Personen');
                $form->message      = Input::get('Bericht');
            $form->save();

            if(Input::get('Type') == 'contact') {
                Session::flash('message', 'Uw bericht is verstuurd');
            } elseif(Input::get('Type') == 'event') {
                Session::flash('message', 'Uw inschrijving is verstuurd');
            } elseif(Input::get('Type') == 'stage') {
                Session::flash('message', 'Uw inschrijving is verstuurd');
            } elseif(Input::get('Type') == 'start') {
                Session::flash('message', 'Uw aanvraag is verstuurd');
            } else {
                Session::flash('message', 'Oeps, er is iets misgelopen, probeer opnieuw');
            }

            return Redirect::back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blog = Blog::find($id);
        $comments = DB::table('comments')->where('post_id', '=', $id)->get();

        return view('blog.show',[
            'blog'          => $blog,
            'comments'      => $comments,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
