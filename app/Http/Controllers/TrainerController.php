<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Comment;
use App\Trainer;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class TrainerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $trainers = Trainer::all();
//
//        return view('backend.trainers.index',[
//            'trainers'      => $trainers,
//        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
            $trainer = Trainer::find($id);

            return view('trainers.show',[
                'trainer'       => $trainer,
            ]);
    }
}
