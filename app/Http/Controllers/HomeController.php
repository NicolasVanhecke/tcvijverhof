<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Event;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
//    /**
//     * Create a new controller instance.
//     *
//     * @return void
//     */
//    public function __construct()
//    {
//        $this->middleware('auth');
//    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $event = Event::all();
        $event = DB::table('events')->orderBy('created_at', 'DESC')->take(2)->get();

        return view('index',[
            'event'      => $event,
        ]);
    }
}
