<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Comment;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class BackUsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->isMedewerker() or Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            $users = User::all();

            return view('backend.users.index',[
                'users'     =>  $users,
            ]);
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            return view('backend.users.create');
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            $rules = array(
                'name'      => 'required|max:255|unique:users',
    //            'firstname' => 'required|max:255',
    //            'lastname'  => 'required|max:255',
    //            'cellphone' => 'required|max:20',
                'email'     => 'required|email|max:255|unique:users',
            );
            $validator = Validator::make(Input::all(), $rules);

            if ($validator->fails()) {
                return Redirect::back()
                    ->withErrors($validator)
                    ->withInput();
            } else {
                $user = new User();
                $user->name         = Input::get('name');
    //            $user->firstname    = Input::get('firstname');
    //            $user->lastname     = Input::get('lastname');
    //            $user->cellphone    = Input::get('cellphone');
                $user->email        = Input::get('email');
                $user->save();

                Session::flash('message', 'Gebruiker aangemaakt');
                return Redirect::to('backend/gebruikers');
            }
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            $user = User::find($id);

            return view('backend.users.show',[
                'user'      =>  $user,
            ]);
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            // get the user
            $user = User::find($id);

            return view('backend.users.edit',
            [
                'user'      => $user,
            ]);
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            // validate
            $rules = array(
                'name'      => 'required|min:6|max:255|unique:users,name,'.$id,
                'email'     => 'required|email|max:255|unique:users,email,'.$id,
            );
            $validator = Validator::make(Input::all(), $rules);

            // process the login
            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator);
            } else {
                // store
                $user = User::find($id);
                $user->name         = Input::get('name');
                $user->email        = Input::get('email');
                $user->save();

                // redirect
                Session::flash('message', 'Gebruiker aangepast');
                return Redirect::to('backend/gebruikers');
            }
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            $user = User::find($id);
            $user->delete();

            Session::flash('message', 'Gebruiker verwijderd');
            return Redirect::back();
        } else{ return Redirect::to('permission'); }
    }
}
