<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Comment;
use App\Role;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class BackRolesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            $medewerkers = Role::with('users')->where('name', 'medewerker')->get();
            $bazen = Role::with('users')->where('name', 'baas')->get();
            $admins = Role::with('users')->where('name', 'admin')->get();

            return view('backend.roles.index',[
                'medewerkers'   =>  $medewerkers,
                'bazen'         =>  $bazen,
                'admins'        =>  $admins,
            ]);
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            $users = User::all();

            return view('backend.roles.create', [
                'users'     => $users,
            ]);
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->isMedewerker() or Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            // validate
            // read more on validation at http://laravel.com/docs/validation
            $rules = array(
                'name'      => 'required|max:255|unique:users',
            );
            $validator = Validator::make(Input::all(), $rules);

            // process the login
            if ($validator->fails()) {
                return Redirect::to('users/create')->withErrors($validator);
            } else {
                // store
                $user = new User();
                $user->name         = Input::get('name');
                $user->firstname    = Input::get('firstname');
                $user->lastname     = Input::get('lastname');
                $user->state        = Input::get('state');
                $user->cellphone    = Input::get('cellphone');
                $user->rank         = Input::get('rank');
                $user->club         = Input::get('club');
                $user->email        = Input::get('email');
                $user->password     = bcrypt(Input::get('password'));
                $user->save();

                // redirect
                Session::flash('message', 'You have done something correct. Congrats!');
                return Redirect::to('roles');
            }
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->isMedewerker() or Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            $user = User::find($id);

            return view('backend.roles.edit',
                [
                    'user'      => $user,
                ]);
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth::user()->isMedewerker() or Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            $user = User::find($id);
            $roleId = Input::get('roleId');
            $status = Input::get('status');
            if($status == 'grant') {
                $user->roles()->attach($roleId);
            }
            else {
                $user->roles()->detach($roleId);
            }
            $user->save();

            // redirect
            Session::flash("message", "Gebruikers rol aangepast");
            return Redirect::to('backend/rollen/' . $user->id . '/edit');
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->isMedewerker() or Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            $user = User::find($id);
            $user->delete();

            Session::flash('message', 'Gebruiker verwijderd');
            return Redirect::back();
        } else{ return Redirect::to('permission'); }
    }
}
