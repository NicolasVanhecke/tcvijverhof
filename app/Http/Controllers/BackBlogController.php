<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Comment;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class BackBlogController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->isMedewerker() or Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            $blog = Blog::all();

            return view('backend.blog.index', [
                'blog' => $blog,
            ]);
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->isMedewerker() or Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            return view('backend.blog.create');
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->isMedewerker() or Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            $rules = array(
                'name'      => 'required|max:255',
                'pre'       => 'required|min:50|max:300',
                'body'      => 'required|min:100',
                'date'      => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);

            if ($validator->fails()) {
                return Redirect::back()
                    ->withErrors($validator)
                    ->withInput();
            } else {
                $blog = new Blog();
                $blog->name         = Input::get('name');
                $blog->pre          = Input::get('pre');
                $blog->body         = Input::get('body');
                $blog->date         = Input::get('date');
                $blog->save();

                Session::flash('message', 'Blogpost aangemaakt');
                return Redirect::to('backend/blog');
            }
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::user()->isMedewerker() or Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            $blog = Blog::find($id);
            $comments = Comment::with('blog')->get();
            $commentsnr = DB::table('comments')->where('blog_id', '=', $id)->count();

            return view('backend.blog.show',[
                'blog'          => $blog,
                'comments'      => $comments,
                'commentsnr'    => $commentsnr,
            ]);
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->isMedewerker() or Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            $blog = Blog::find($id);

            return view('backend.blog.edit', [
                'blog'      => $blog,
            ]);
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth::user()->isMedewerker() or Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            // validate
            $rules = array(
                'name'      => 'required|max:255',
                'pre'       => 'required|min:50|max:300',
                'body'      => 'required|min:100',
                'date'      => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);

            // process the login
            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator);
            } else {
                // store
                $blog = Blog::find($id);
                $blog->name         = Input::get('name');
                $blog->pre          = Input::get('pre');
                $blog->body         = Input::get('body');
                $blog->date         = Input::get('date');
                $blog->save();

                // redirect
                Session::flash('message', 'Blogpost aangepast');
                return Redirect::to('backend/blog');
            }
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            $blog = Blog::find($id);
            $blog->delete();

            Session::flash('message', 'Blogpost verwijderd');
            return Redirect::back();
        } else{ return Redirect::to('permission'); }
    }
}
