<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Comment;
use App\Event;
use App\Form;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class BackEventsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->isMedewerker() or Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            $event = Event::all();
            $forms = Form::with('event')->get();

            return view('backend.events.index',[
                'event'     => $event,
                'forms'     => $forms,
            ]);
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->isMedewerker() or Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            return view('backend.events.create');
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->isMedewerker() or Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            $rules = array(
                'name'      => 'required|max:255',
                'pre'       => 'required|min:50|max:300',
                'body'      => 'required|min:100',
                'date'      => 'required',
                'price'     => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);

            if ($validator->fails()) {
                return Redirect::back()
                    ->withErrors($validator)
                    ->withInput();
            } else {
                $event = new Event();
                $event->name         = Input::get('name');
                $event->pre          = Input::get('pre');
                $event->body         = Input::get('body');
                $event->date         = Input::get('date');
                $event->price        = Input::get('price');
                $event->save();

                Session::flash('message', 'Evenement aangemaakt');
                return Redirect::to('backend/evenementen');
            }
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::user()->isMedewerker() or Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            $event = Event::find($id);
            $forms = Form::with('event')->get();
            $inschrijvingsnr = DB::table('forms')->where('event_id', '=', $id)->count();
//            dd($forms);

            return view('backend.events.show',[
                'event'             => $event,
                'forms'             => $forms,
                'inschrijvingsnr'   => $inschrijvingsnr,
            ]);
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->isMedewerker() or Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            $event = Event::find($id);

            return view('backend.events.edit',[
                'event'         => $event,
            ]);
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth::user()->isMedewerker() or Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            // validate
            $rules = array(
                'name'      => 'required|max:255',
                'pre'       => 'required|min:50|max:300',
                'body'      => 'required|min:100',
                'date'      => 'required',
                'price'     => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);

            // process the login
            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator);
            } else {
                $event = Event::find($id);
                $event->name         = Input::get('name');
                $event->pre          = Input::get('pre');
                $event->body         = Input::get('body');
                $event->date         = Input::get('date');
                $event->price        = Input::get('price');
                $event->save();

                Session::flash('message', 'Evenement aangemaakt');
                return Redirect::to('backend/evenementen');
            }
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            $event = Event::find($id);
            $event->delete();

            Session::flash('message', 'Evenement verwijderd');
            return Redirect::back();
        } else{ return Redirect::to('permission'); }
    }
}
