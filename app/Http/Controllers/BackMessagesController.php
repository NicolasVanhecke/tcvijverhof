<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Comment;
use App\Form;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class BackMessagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->isMedewerker() or Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            $forms = Form::all();
            $msg = DB::table('forms')->where('type', '=', 'contact')->get();

            return view('backend.messages.index',[
                'forms'         => $forms,
                'msg'           => $msg,
            ]);
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::user()->isMedewerker() or Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            $msg = Form::find($id);

            return view('backend.messages.show',[
                'msg'           => $msg,
            ]);
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            $msg = Form::find($id);
            $msg->delete();

            Session::flash('message', 'Bericht verwijderd');
            return Redirect::back();
        } else{ return Redirect::to('permission'); }
    }
}
