<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Comment;
use App\Trainer;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class BackTrainersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->isMedewerker() or Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            $trainers = Trainer::all();

            return view('backend.trainers.index',[
                'trainers'      => $trainers,
            ]);
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            return view('backend.trainers.create');
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            $rules = array(
                'firstname' => 'required|max:24',
                'lastname'  => 'required|max:24',
                'niveau'    => 'required|max:64',
                'cellphone' => 'required|max:20',
                'email'     => 'required|email|max:255',
                'stroke'    => 'required|max:32',
                'likes'     => 'required|max:64',
                'message'   => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);

            if ($validator->fails()) {
                return Redirect::back()
                    ->withErrors($validator)
                    ->withInput();
            } else {
                $trainer = new Trainer();
                $trainer->firstname    = Input::get('firstname');
                $trainer->lastname     = Input::get('lastname');
                $trainer->niveau       = Input::get('niveau');
                $trainer->email        = Input::get('email');
                $trainer->cellphone    = Input::get('cellphone');
                $trainer->stroke       = Input::get('stroke');
                $trainer->likes        = Input::get('likes');
                $trainer->message      = Input::get('message');

                if(Input::hasFile('photo'))
                {
                    $name = Input::get('firstname') . Input::get('lastname');
                    $photo = Input::file('photo')->move(public_path().'/assets/photos/', time().'-'.$name.'.png');
                    $trainer->photo  = time().'-'.$name.'.png';
                }

                $trainer->save();

                Session::flash('message', 'Trainer aangemaakt');
                return Redirect::to('backend/trainers');
            }
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Auth::user()->isMedewerker() or Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            $trainer = Trainer::find($id);

            return view('backend.trainers.show',[
                'trainer'       => $trainer,
            ]);
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            $trainer = Trainer::find($id);

            return view('backend.trainers.edit', [
                'trainer'       => $trainer,
            ]);
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            // validate
            $rules = array(
                'firstname' => 'required|max:24',
                'lastname'  => 'required|max:24',
                'niveau'    => 'required|max:64',
                'cellphone' => 'required|max:20',
                'email'     => 'required|email|max:255',
                'stroke'    => 'required|max:32',
                'likes'     => 'required|max:64',
                'message'   => 'required',
    //            'photo'     => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);

            // process the login
            if ($validator->fails()) {
                return Redirect::back()->withErrors($validator);
            } else {
                $trainer = Trainer::find($id);
                $trainer->firstname    = Input::get('firstname');
                $trainer->lastname     = Input::get('lastname');
                $trainer->niveau       = Input::get('niveau');
                $trainer->email        = Input::get('email');
                $trainer->cellphone    = Input::get('cellphone');
                $trainer->stroke       = Input::get('stroke');
                $trainer->likes        = Input::get('likes');
                $trainer->message      = Input::get('message');

                if(Input::hasFile('photo'))
                {
                    $name = Input::get('firstname') . Input::get('lastname');
                    $photo = Input::file('photo')->move(public_path().'/assets/photos/', time().'-'.$name.'.png');
                    $trainer->photo  = time().'-'.$name.'.png';
                }

                $trainer->save();

                Session::flash('message', 'Trainer aangepast');
                return Redirect::to('backend/trainers');
            }
        } else{ return Redirect::to('permission'); }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Auth::user()->isBaas() or Auth::user()->isAdmin()) {
            $trainer = Trainer::find($id);
            $trainer->delete();

            Session::flash('message', 'Trainer verwijderd');
            return Redirect::back();
        } else{ return Redirect::to('permission'); }
    }
}
