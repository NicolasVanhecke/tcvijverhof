<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Comment;
use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blog = Blog::all();
//        dd($blog);

        return view('blog.index',[
            'blog'      => $blog,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        return view('blog.show');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
////        $rules = array(
////            'body'      => 'required|min:6',
////        );
////        $validator = Validator::make(Input::all(), $rules);
//
////        if ($validator->fails()) {
////            return Redirect::back()
////                ->withErrors($validator)
////                ->withInput();
////        } else {
//            // write to sql database for filtering
//            $comment = new Comment();
//            $comment->body      = Input::get('Bericht');
//            $comment->post_id   = Input::get('postId');
//            $comment->user_id   = Input::get('userId');
//            $comment->save();
//
//            Session::flash('message', 'Uw reactie is geplaatst');
//            return Redirect::back();
////        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blog = Blog::find($id);
        $comments = Comment::with('blog')->get();
        $commentsnr = DB::table('comments')->where('blog_id', '=', $id)->count();

        return view('blog.show',[
            'blog'          => $blog,
            'comments'      => $comments,
            'commentsnr'    => $commentsnr,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
