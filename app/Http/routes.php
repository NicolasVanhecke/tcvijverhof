<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => 'web'], function () {
    // Unauthorized

    // BACKEND
    // -------
    Route::resource('/', 'HomeController');
    Route::resource('home', 'HomeController');
    Route::resource('blog', 'BlogController');
    Route::resource('evenementen', 'EventController');
    Route::resource('tennisschool', 'SchoolController');
    Route::resource('trainers', 'TrainerController');
    Route::resource('club', 'ClubController');
    Route::resource('contact', 'ContactController');

    Route::get('school/start2tennis', 'SchoolController@start2tennis');
    Route::get('school/kids', 'SchoolController@kids');
    Route::get('school/stages', 'SchoolController@stages');
    Route::get('school/lessen', 'SchoolController@lessen');

    Route::resource('comment', 'CommentController');
    Route::resource('form', 'FormController');


    // BACKEND
    // -------
    Route::resource('backend/dash', 'BackDashController');
    Route::resource('backend/gebruikers', 'BackUsersController');
    Route::resource('backend/blog', 'BackBlogController');
    Route::resource('backend/evenementen', 'BackEventsController');
    Route::resource('backend/trainers', 'BackTrainersController');
    Route::resource('backend/berichten', 'BackMessagesController');
    Route::resource('backend/inschrijvingen', 'BackParticipantsController');
    Route::resource('backend/rollen', 'BackRolesController');


    // No idea
    // --------
    Route::auth();

    // Authorized by Login

});
